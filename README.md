This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `yarn build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify

Windframe: https://devwares.com/Windframe


## Documentation

- [Check out our documentation here](https://www.devwares.com/docs/contrast/react/index)
- [Accordion](https://www.devwares.com/docs/contrast/react/components/accordion/)
- [Alert](https://www.devwares.com/docs/contrast/react/components/alert/)
- [Autocomplete](https://www.devwares.com/docs/contrast/react/components/autocomplete/)
- [Animation](https://www.devwares.com/docs/contrast/react/components/animations/)
- [Badges](https://www.devwares.com/docs/contrast/react/components/badge/)
- [Breadcrumb](https://www.devwares.com/docs/contrast/react/components/breadcrumb/)
- [ButtonToolbar](https://www.devwares.com/docs/contrast/react/components/buttonToolbar/)
- [Box](https://www.devwares.com/docs/contrast/react/components/box/)
- [ButtonGroup](https://www.devwares.com/docs/contrast/react/components/buttonGroup/)
- [Checkbox](https://www.devwares.com/docs/contrast/react/components/checkbox/)
- [Carousel](https://www.devwares.com/docs/contrast/react/components/carousel/)
- [Button](https://www.devwares.com/docs/contrast/react/components/buttons/)
- [Collapse](https://www.devwares.com/docs/contrast/react/components/collapse/)
- [Icon](https://www.devwares.com/docs/contrast/react/components/icon/)
- [Footer](https://www.devwares.com/docs/contrast/react/components/footer/)
- [Iframe](https://www.devwares.com/docs/contrast/react/components/iframe/)
- [Input](https://www.devwares.com/docs/contrast/react/components/input/)
- [Card](https://www.devwares.com/docs/contrast/react/components/card/)
- [Mask](https://www.devwares.com/docs/contrast/react/components/mask/)
- [ListGroup](https://www.devwares.com/docs/contrast/react/components/listgroup/)
- [InputGroup](https://www.devwares.com/docs/contrast/react/components/inputgroup/)
- [Multiselect](https://www.devwares.com/docs/contrast/react/components/multiselect/)
- [Notification](https://www.devwares.com/docs/contrast/react/components/notification/)
- [Pane](https://www.devwares.com/docs/contrast/react/components/pane/)
- [DropDown](https://www.devwares.com/docs/contrast/react/components/dropdown/)
- [Modal](https://www.devwares.com/docs/contrast/react/components/modal/)
- [Progress](https://www.devwares.com/docs/contrast/react/components/progress/)
- [Rating](https://www.devwares.com/docs/contrast/react/components/rating/)
- [Radio](https://www.devwares.com/docs/contrast/react/components/radio/)
- [Panel](https://www.devwares.com/docs/contrast/react/components/panels/)
- [Select](https://www.devwares.com/docs/contrast/react/components/select/)
- [Slider](https://www.devwares.com/docs/contrast/react/components/slider/)
- [SmoothScroll](https://www.devwares.com/docs/contrast/react/components/smoothscroll/)
- [Select 2](https://www.devwares.com/docs/contrast/react/components/select2/)
- [Spinner](https://www.devwares.com/docs/contrast/react/components/spinner/)
- [Switch](https://www.devwares.com/docs/contrast/react/components/switch/)
- [Stepper](https://www.devwares.com/docs/contrast/react/components/stepper/)
- [Charts](https://www.devwares.com/docs/contrast/react/chart/)
- [Navigation](https://www.devwares.com/docs/contrast/react/navigation/)
- [DatePicker](https://www.devwares.com/docs/contrast/react/sections/datepicker/)
- [FileUploader](https://www.devwares.com/docs/contrast/react/sections/fileuploader/)
- [TimePicker](https://www.devwares.com/docs/contrast/react/sections/timepicker/)
- [EditableTable](https://www.devwares.com/docs/contrast/react/table/editabletable/)
- [Table](https://www.devwares.com/docs/contrast/react/table/table/)
- [DataTable](https://www.devwares.com/docs/contrast/react/table/datatables/)
- [Widgets](https://www.devwares.com/docs/contrast/react/widgets/)
- [Forms](https://www.devwares.com/docs/contrast/react/forms/)

## Blog articles


- [How to create Tailwind CSS Modal](https://www.devwares.com/blog/how-to-create-tailwind-css-modal)

- [How to use tailwind CSS in HTML](https://www.devwares.com/blog/how-to-use-tailwind-css-in-HTML)

- [Get Contrast PRO](https://www.devwares.com/product/contrast-pro)

- [Why tailwind CSS is good](https://www.devwares.com/blog/why-tailwind-css-is-good)

- [How to use Tailwind CSS in Nextjs](https://www.devwares.com/blog/tailwind-css-in-nextjs)

- [How to create Tailwind CSS Components for your Website](https://www.devwares.com/blog/how-to-create-tailwind-css-components-for-your-website)

- [How to create Tailwind CSS Animation](https://www.devwares.com/blog/create-animation-with-tailwind-css/)

- [Tailwind grid-How to use tailwind CSS grid templates in your project](https://www.devwares.com/blog/how-to-add-tailwind-css-grid-to-your-project/)

- [How to create a Beautiful Responsive Navigation bar Using Tailwind CSS](https://www.devwares.com/blog/how-to-create-a-beautiful-responsive-navbar-using-tailwind-css/)

- [Tailwind form-How to create and style a Responsive Form using Tailwind CSS](https://www.devwares.com/blog/how-to-create-and-style-a-responsive-form-using-tailwindcss/)

- [Tailwind CSS Flex: How to use Tailwind CSS Flex](https://www.devwares.com/blog/how-to-use-tailwind-css-flex/)

- [How to use tailwind CSS padding, margin and border in your project](https://www.devwares.com/blog/how-to-use-tailwind-css-padding-margin-and-border-in-your-project/)

- [Tailwind CSS CDN-How to use the Tailwind CSS JIT CDN](https://www.devwares.com/blog/how-to-use-the-tailwind-css-JIT-CDN/)

- [How to set up your first Tailwind CSS Project](https://www.devwares.com/blog/setting-up-your-first-project-using-tailwind-css/)

- [How to use Tailwind CSS in HTML](https://www.devwares.com/blog/how-to-use-tailwind-css-in-HTML/)

- [Tailwind CSS table-How to Create Tailwind CSS tables](https://www.devwares.com/blog/how-to-create-tailwind-css-tables/)

- [How to set up your first Tailwind CSS Project](https://www.devwares.com/blog/setting-up-your-first-project-using-tailwind-css/)

- [Why is tailwind CSS better than other CSS framework](https://www.devwares.com/blog/why-tailwind-css-is-good/)

- [10 tailwind CSS templates and themes](https://www.devwares.com/blog/tailwind-css-10-templates-and-themes/)

- [How to add tailwind CSS colors and Fonts to your project](https://www.devwares.com/blog/how-to-add-tailwind-css-colors-and-fonts-to-your-project/)

- [Differences between Tailwind CSS and SASS](https://www.devwares.com/blog/differences-between-tailwind-css-and-sass/)

- [Differences Between Tailwind CSS and Bootstrap](https://www.devwares.com/blog/diffrences-between-tailwind-css-and-bootstrap/)

- [10 Awesome projects built with Tailwind CSS](https://www.devwares.com/blog/awesome-10-projects-built-with-tailwind/).

- [How to install Tailwind CSS in Vue.js](https://www.devwares.com/blog/how-to-install-tailwind-css-in-vuejs/).

- [How to use Tailwind CSS in React](https://www.devwares.com/blog/how-to-use-tailwind-css-in-react/)

- [How to install Tailwind CSS with Laravel](https://www.devwares.com/blog/how-to-install-tailwind-css-in-laravel/)

- [How to create react date picker](https://www.devwares.com/blog/create-datepicker-with-contrast/)

- [React bootstrap 5 form-How to create React forms](https://www.devwares.com/blog/how-to-create-and-style-a-responsive-form-using-tailwindcss/).

- [How to create a beautiful React multiselect](https://www.devwares.com/blog/create-multiselect-with-contrast/).

- [How to create a beautiful React Bootstrap progress bar](https://www.devwares.com/blog/create-progress-with-contrast/).

- [How to create a beautiful React Bootstrap select with icons](https://www.devwares.com/blog/create-select-with-contrast/).

- [How to create a beautiful Bootstrap 5 stepper](https://www.devwares.com/blog/create-stepper-with-contrast/)

- [How to create a beautiful React Bootstrap table](https://www.devwares.com/blog/create-tables-with-contrast/)

- [How to create beautiful React Bootstrap tabs](https://www.devwares.com/blog/create-tabs-with-contrast/)

- [How to create a Beautiful Responsive Navigation bar Using Tailwind CSS](https://www.devwares.com/blog/how-to-create-a-beautiful-responsive-navbar-using-tailwind-css/)

- [Tailwind Modal-How to create a React Modal using Tailwind CSS.](https://www.devwares.com/blog/how-to-create-a-react-modal-using-tailwind-css/)

- [How to create a Bootstrap 5 Modal.](https://www.devwares.com/blog/how-to-create-a-bootstrap5-modal/)

- [How to use Tailwind CSS Width](https://www.devwares.com/blog/Tailwind-width/)
- [Tailwind CSS Colors 3.0](https://www.devwares.com/blog/How-to-add-color-to-Tailwind/) 
- [How to build Tailwind css timepicker with Tailwind elements](https://www.devwares.com/blog/how-to-build-tailwindcss-timepicker-with-tailwind-element/)
- [How to Create a Responsive React Sidebar Design Using Tailwind CSS.](https://www.devwares.com/blog/how-to-create-a-responsive-react-sidebar-design-using-tailwind-css/)
- [How to implement dark mode in React using tailwind css.](https://www.devwares.com/blog/how-to-implement-dark-mode-in-tailwind-css/)
- [How to create a beautiful Bootstrap Datatable with Icons](https://www.devwares.com/blog/create-datatable-with-contrast/)
- [How to create a React datepicker using React Bootstrap.](https://www.devwares.com/blog/create-datepicker-with-contrast/)
- [React bootstrap 5 form-How to create React forms](https://www.devwares.com/blog/create-forms-with-contrast/)
- [How to create a beautiful React multiselect.](https://www.devwares.com/blog/create-multiselect-with-contrast/)
- [How to create a beautiful React Bootstrap](https://www.devwares.com/blog/create-progress-with-contrast/)
- [How to create a responsive React Bootstrap Sidebar](https://www.devwares.com/blog/create-responsive-sidebar-in-react/)
- [How to create a beautiful React Bootstrap select with icons.](https://www.devwares.com/blog/create-select-with-contrast/)
- [Bootstrap 5 stepper-How to create a beautiful Bootstrap 5 stepper](https://www.devwares.com/blog/create-stepper-with-contrast/)
- [How to create a beautiful React Bootstrap table.](https://www.devwares.com/blog/create-tables-with-contrast/)
- [How to create beautiful React Bootstrap tabs](https://www.devwares.com/blog/create-tabs-with-contrast/)
- [How To Create A Datatable Using Angular Bootstrap](https://www.devwares.com/blog/create-datatable-in-angular-10/)
- [How To Create Bootstrap Charts using Bootstrap 5](https://www.devwares.com/blog/create-bootstrap-charts-using-bootstrap5/)
- [How To Create A Beautiful Navbar Using Bootstrap 5.](https://www.devwares.com/blog/bootstrap-5-navbar-using-contrast/)
- [Major changes to bootstrap 5](https://www.devwares.com/blog/bootstrap-5-major-changes/)
- [All you need to know about Bootstrap 5](https://www.devwares.com/blog/bootstrap-5-all-you-need-to-know/)
