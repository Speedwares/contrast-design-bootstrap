/* import React, { Component, useState } from "react";
import { Transition } from "react-spring/renderprops";
import { Manager, Reference, Popper } from "react-popper";
import styled from "@emotion/styled";
import { keyframes } from "@emotion/core";

export const Main = styled("main")`
  overflow: hidden;
  min-height: 30em;
  display: flex;
  width: 90%;
  margin: 0 auto;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 100vh;
  background: green;
  color: #ffffff;
`;

export const ReferenceBox = styled("div")`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 10em;
  height: 6em;
  background-color: #ffffff;
  color: #000000;
  border-radius: 4px;
  z-index: 1;
  position: relative;
  a {
    color: #000000;
  }
`;

export const ClickableReferenceBox = styled(ReferenceBox)`
  cursor: pointer;
`;

export const PopperBox = styled("div")`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 6em;
  height: 6em;
  background-color: #232323;
  color: #ffffff;
  border-radius: 10px;
  padding: 0.5em;
  text-align: center;
  ${(props) => props.popperStyle};
`;

export const Arrow = styled("div")`
  position: absolute;
  width: 3em;
  height: 3em;
  &[data-placement*="bottom"] {
    top: 0;
    left: 0;
    margin-top: -0.3em;
    &::before {
      border-width: 0 0.5em 0.5em 0.5em;
      border-color: transparent transparent #232323 transparent;
    }
  }
  &[data-placement*="top"] {
    bottom: 0;
    left: 0;
    margin-bottom: -2.9em;
    &::before {
      border-width: 0.5em 0.5em 0 0.5em;
      border-color: #232323 transparent transparent transparent;
    }
  }
  &[data-placement*="right"] {
    left: 0;
    margin-left: -0.63em;
    &::before {
      border-width: 0.5em 0.5em 0.5em 0;
      border-color: transparent #232323 transparent transparent;
    }
  }
  &[data-placement*="left"] {
    right: 0;
    margin-right: -0.63em;
    &::before {
      border-width: 0.5em 0 0.5em 0.5em;
      border-color: transparent transparent transparent #232323;
    }
  }
  &::before {
    content: "";
    margin: auto;
    display: block;
    width: 0;
    height: 0;
    border-style: solid;
  }
`;
const Null = () => null;

const modifiers = [
  {
    name: "flip",
    enabled: false,
  },
  {
    name: "hide",
    enabled: false,
  },
];

const popperModifiers = [
  ...modifiers,
  {
    name: "arrow",
    options: {
      padding: 5,
    },
  },
  {
    name: "offset",
    options: {
      offset: [0, 14],
    },
  },
];

const dotModifiers = [
  ...modifiers,
  {
    name: "offset",
    options: {
      offset: [0, 56],
    },
  },
];

const animatedModifiers = [
  ...popperModifiers,
  {
    name: "computeStyles",
    options: {
      adaptive: false,
    },
  },

  {
    name: "computeStyles",
    options: {
      gpuAcceleration: false,
    },
  },
];

export const Example = () => {
  const [shouldShowElement, setShouldShowElement] = useState(false);
  return (
    <Main>
      <Manager>
        <Reference>
          {({ ref }) => (
            <button
              type="button"
              ref={ref}
              onClick={() => {
                setShouldShowElement(!shouldShowElement);
              }}
            >
              Reference element
            </button>
          )}
        </Reference>
        <Transition
          items={shouldShowElement}
          from={{ opacity: 0, scale: 0.8, top: 0 }}
          enter={{ opacity: 1, scale: 1, top: 0 }}
          leave={{ opacity: 0, scale: 0.8, top: 0 }}
        >
          {(show) =>
            show
              ? ({ scale, opacity, top: topOffset }) => (
                  <Popper placement="left" modifiers={animatedModifiers}>
                    {({
                      ref,
                      style: { top, left, position },
                      placement,
                      arrowProps,
                    }) => (
                      <PopperBox
                        ref={ref}
                        style={{
                          opacity,
                          top: 0,
                          left: 0,
                          position,
                          padding: "1em",
                          width: "10em",
                          transform: `translate3d(${left}, ${
                            parseInt(top) + topOffset
                          }px, 0) scale(${scale})`,
                          transformOrigin: "top center",
                        }}
                        data-placement={placement}
                      >
                        Popper element
                        <Arrow
                          ref={arrowProps.ref}
                          style={arrowProps.style}
                          data-placement={placement}
                        />
                      </PopperBox>
                    )}
                  </Popper>
                )
              : Null
          }
        </Transition>
      </Manager>
    </Main>
  );
};
 */
