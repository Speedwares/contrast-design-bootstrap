import styled from "styled-components";

export const StyledLayout = styled.div`
  width: 80%;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  .blockcode {
    margin: 20px 0;
  }
  .header {
    font-size: 20px;
    margin: 10px 0;
  }
  .description {
    font-size: 15px;
    margin: 10px 0;
  }
  .example {
    border: 1px solid #f4f4f4;
    margin: 0 20px;
    display: flex;
    padding: 20px;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    & > * {
      margin: 15px;
    }
  }
  .example2 {
    border: 1px solid #f4f4f4;
    margin: 0 20px;
    display: flex;
    padding: 20px;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    & > * {
      width: 100%;
      display: flex;
      justify-content: center;
    }
  }
`;
