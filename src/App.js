import React from "react";
import { ThemeProvider } from "styled-components";
import { theme } from "./theme";
import Routes from "./routes";
import "./App.css";

function App() {
  return (
    <ThemeProvider theme={theme}>
      <div className="App">
        <Routes />
      </div>
    </ThemeProvider>
  );
}

export default App;
