import React, { useContext } from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
import { Reference } from "react-popper";
import { Component } from "./DropDownToggle.style";
import { DropDownContext } from "../DropDownContext";

const DropdownToggle = (props) => {
  const { isOpen } = useContext(DropDownContext);
  const [isOpenValue, setIsOpenValue] = isOpen;

  const { className, tag, color, children, caret, nav, size, disabled } = props;
  const dropdownToggleClasses = classNames(
    {
      "dropdown-toggle": caret,
      "nav-link": nav,
    },
    color && `cdb-dropdown-toggle-${color}`,
    className
  );
  const toggleDropdown = (e) => {
    const { disabled, nav, tag, onClick } = props;

    if (disabled) {
      return;
    }

    if (nav && !tag) {
      return;
    }

    if (onClick) {
      onClick(e);
    }

    setIsOpenValue(!isOpenValue);
  };
  let dropdownToggleComponent = (
    <Reference>
      {({ ref }) => (
        <Component
          className={dropdownToggleClasses}
          ref={ref}
          as={tag}
          colors={color}
          size={size}
          disabled={disabled}
          onClick={() => {
            toggleDropdown();
          }}
        >
          {children}
        </Component>
      )}
    </Reference>
  );

  return dropdownToggleComponent;
};

DropdownToggle.defaultProps = {
  "aria-haspopup": true,
  color: "primary",
  tag: "button",
};

DropdownToggle.propTypes = {
  className: PropTypes.string,
  colors: PropTypes.oneOf([
    "primary",
    "secondary",
    "success",
    "danger",
    "warning",
    "info",
  ]),
  position: PropTypes.string,
  children: PropTypes.node,
  caret: PropTypes.bool,
  size: PropTypes.string,
  nav: PropTypes.bool,
  tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
};

export default DropdownToggle;
export { DropdownToggle as CDBDropdownToggle };
