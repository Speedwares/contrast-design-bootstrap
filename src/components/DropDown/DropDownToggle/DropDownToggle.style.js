import styled from "styled-components";
import { colorStyle, space, variant } from "styled-system";

export const Component = styled("button")(
  colorStyle,
  space,
  {
    padding: "10px",
    border: "none",
  },
  variant({
    prop: "size",
    variants: {
      lg: {
        fontSize: "1.25rem",
        padding: "20px",
      },
      sm: {
        fontSize: "0.75rem",
        padding: "5px",
      },
    },
  })
);
