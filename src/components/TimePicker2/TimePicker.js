import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
import { Component } from "./TimePicker.style";
import TimePicker from "react-times";
import "./TimePicker.material.css";

const TimePickerComponent = (props) => {
  const { className, tag, colorPalette, ...attrs } = props;

  const timePickerClasses = classNames("time-picker", className);

  let timePickerComp = (
    <Component as={tag} className={timePickerClasses} {...attrs}>
      <TimePicker
        showTimezone
        focused
        withoutIcon
        colorPalette={colorPalette}
        theme="material"
        timeMode="12"
        timezone="America/New_York"
      />
    </Component>
  );
  return timePickerComp;
};

TimePickerComponent.defaultProps = {
  tag: "div",
  colorPalette: "white",
};

TimePickerComponent.propTypes = {
  className: PropTypes.string,
  colorPalette: PropTypes.oneOf(["light", "dark"]),
  theme: PropTypes.oneOf(["material", "classic", "neomorphism"]),
  tag: PropTypes.string,
};
export default TimePickerComponent;

export { TimePickerComponent as CDBTimePicker };
