import React, { Component } from "react";
import PropTypes from "prop-types";
import ReactTooltip from "react-tooltip";
import CDBIcon from "../Icon";
import CDBBtn from "../Button";

export const Step = ({ name, icon, children, far, fab, fas }) => {
  const iconType = fab ? "fab" : far ? "far" : fas ? "fas" : null;
  return (
    <div className="steps" key={name} data-tip data-for={`tip ${name}`}>
      {icon ? (
        <CDBBtn className="default-node" color="dark">
          <CDBIcon icon={icon} fab={fab} far={far} fas={fas} />
        </CDBBtn>
      ) : (
        { children }
      )}

      <ReactTooltip id={`tip ${name}`} place="top" effect="solid">
        {name}
      </ReactTooltip>
    </div>
  );
};

Step.defaultProps = {};

Step.propTypes = {
  name: PropTypes.string,
  icon: PropTypes.string,
};

export default Step;

export { Step as CDBStep };
