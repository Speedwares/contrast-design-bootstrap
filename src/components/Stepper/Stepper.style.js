import styled, { css } from "styled-components";

export const Component = styled.div`
  ${(props) =>
    props.direction === "vertical"
      ? css`
          width: 100px;
          height: 100%;
          margin-top: 22px;
          position: relative;
          .step-row-2 {
            display: flex;
            -ms-flex-align: center;
            flex-direction: column;
            align-items: center;
            height: 100%;
            justify-content: space-between !important;
            &:before {
              bottom: 0;
              position: absolute;
              content: "";
              width: 2px;
              height: 100%;
              background-color: #000000;
            }
          }
        `
      : css`
          display: table;
          width: 100%;
          margin-top: 22px;
          position: relative;
          .step-row-2 {
            justify-content: space-between !important;
            display: flex !important;
            &:before {
              top: 14px;
              bottom: 0;
              position: absolute;
              content: " ";
              width: 100%;
              height: 2px;
              background-color: #000000;
            }
          }
        `}

  .steps {
    display: table-cell;
    text-align: center;
    position: relative;
    .box {
      min-width: 100px;
      min-height: 100px;
      color: white;
    }
    color: #fff;
  }

  .steps .default-node {
    width: 70px;
    height: 70px;
    background-color: black !important;
    color: white !important;
    border-radius: 50%;
    padding: 22px 18px 15px 18px;
    margin-top: -22px;
  }
`;
