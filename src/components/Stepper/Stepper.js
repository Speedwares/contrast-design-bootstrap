import React, { Component } from "react";
import PropTypes from "prop-types";
import CDBStep from "../Step";
import { Component as StyledComponent } from "./Stepper.style";

const Stepper = (props) => {
  const { direction, children } = props;
  return (
    <StyledComponent direction={direction}>
      <div className="step-row-2">{children}</div>
    </StyledComponent>
  );
};
Stepper.defaultProps = {
  direction: "horizontal",
};
Stepper.propTypes = {
  direction: PropTypes.string.isRequired,
  currentStepNumber: PropTypes.number.isRequired,
  steps: PropTypes.array.isRequired,
  stepColor: PropTypes.string.isRequired,
  goTo: PropTypes.func,
};
export default Stepper;

export { Stepper as CDBStepper };
