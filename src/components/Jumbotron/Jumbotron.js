import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { Component } from "./Jumbotron.style";

const Jumbotron = (props) => {
  const { className, children, fluid, ...attributes } = props;

  const jumbotronClasses = classNames(
    "jumbotron",
    fluid ? "jumbotron-fluid" : false,
    className
  );
  let jumbotronComponent = (
    <Component
      data-test="jumbotron"
      {...attributes}
      className={jumbotronClasses}
    >
      {children}
    </Component>
  );
  return jumbotronComponent;
};

Jumbotron.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  fluid: PropTypes.bool,
};

export default Jumbotron;
export { Jumbotron as CDBJumbotron };
