import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
import { Component } from "./Alert.style";

const Alert = (props) => {
  const { className, tag, color, children } = props;

  const alertClasses = classNames("alert", className);

  let alertComponent = (
    <Component className={alertClasses} role="alert" as={tag} colors={color}>
      {children}
    </Component>
  );

  return alertComponent;
};

Alert.defaultProps = {
  colors: "primary",
  tag: "span",
};

Alert.propTypes = {
  className: PropTypes.string,
  colors: PropTypes.oneOf([
    "primary",
    "secondary",
    "success",
    "danger",
    "warning",
    "info",
  ]),
  onClose: PropTypes.func,
  onClosed: PropTypes.func,
  tag: PropTypes.string,
};

export default Alert;
export { Alert as CDBAlert };
