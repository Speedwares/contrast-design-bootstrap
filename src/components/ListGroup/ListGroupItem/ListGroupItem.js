import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { Component } from "./ListGroupItem.style";
import { Link } from "react-router-dom";

const ListGroupItem = (props) => {
  let {
    active,
    children,
    className,
    color,
    disabled,
    hover,
    info,
    to,
    tag,
    href,
    ...attributes
  } = props;

  const classes = classNames(
    "list-group-item",
    className,
    active,
    disabled,
    color && `list-group-item-${color}`,
    hover && "list-group-item-action"
  );

  let listGroupItemComponent = href ? (
    <Link
      data-test="list-group-item"
      {...attributes}
      className={classes}
      colors={color}
      href={href}
    >
      {children}
    </Link>
  ) : (
    <Component
      data-test="list-group-item"
      {...attributes}
      className={classes}
      colors={color}
    >
      {children}
    </Component>
  );
  return listGroupItemComponent;
};

ListGroupItem.propTypes = {
  active: PropTypes.bool,
  children: PropTypes.node,
  className: PropTypes.string,
  color: PropTypes.oneOf([
    "primary",
    "secondary",
    "success",
    "danger",
    "warning",
    "info",
    "white",
  ]),
  disabled: PropTypes.bool,
  hover: PropTypes.bool,
  tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
};

ListGroupItem.defaultProps = {
  tag: "li",
  color: "white",
};

export default ListGroupItem;
export { ListGroupItem as CDBListGroupItem };
