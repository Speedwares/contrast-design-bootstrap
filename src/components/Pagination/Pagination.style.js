import styled from "styled-components";
import { colorStyle } from "styled-system";
export const Component = styled.div`
  .page-item {
    ${colorStyle}
    min-width: 50px;
    min-height: 50px;
    display: flex;
    align-items: center;
    justify-content: center;
    text-align: center;
    border-radius: ${(props) => (props.circle ? "50%" : "0")};
    font-size: ${(props) =>
      props.size === "big"
        ? "1.25rem"
        : props.size === "small"
        ? "0.75rem"
        : "1rem"};
  }
`;
