import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { Component } from "./PageLink.style";

const PageLink = (props) => {
  const { children, className, tag, ...attributes } = props;

  const pageLinkClasses = classNames("page-link", className);

  let pageLinkComponent = (
    <Component
      as={tag}
      data-test="page-link"
      {...attributes}
      className={pageLinkClasses}
    >
      {children}
    </Component>
  );

  return pageLinkComponent;
};

PageLink.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
};

PageLink.defaultProps = {
  tag: "a",
};

export default PageLink;
export { PageLink as CDBPageNav };
