import styled from "styled-components";

export const Component = styled.a`
  margin: 0px 7.5px;
  cursor: pointer;
`;
