import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Component } from './CardText.style';

const CardText = props => {
    const { children: textNode, className, muted, small, tag, ...attributes } = props;

    const classes = classNames('card-text', muted && 'text-muted', className);

    const children = small ? <small>{textNode}</small> : textNode;

    return (
        <Component data-test='card-text' as={tag} {...attributes} className={classes}>
            {children}
        </Component>
    );
};

CardText.propTypes = {
    className: PropTypes.string,
    muted: PropTypes.bool,
    small: PropTypes.bool,
    tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string])
};

CardText.defaultProps = {
    tag: 'p'
};

export default CardText;
export { CardText as CDBCardText };