import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Component } from './CardImage.style';

import View from '../../View';

const CardImage = props => {

    const { cascade, className, hover, overlay, src, tag, top, waves, zoom, ...attributes } = props;

    const classes = classNames(top && 'card-img-top', className);


    const innerContent = <Component data-test='card-image' as={tag} src={src} {...attributes} className={classes} />;

    if (src) {
        return (
            <View zoom={zoom} hover={hover} cascade={cascade}>
                <div style={{ touchAction: 'unset' }}>
                    {innerContent}
                </div>
            </View>
        );
    }
    return <div>{innerContent}</div>;
};

CardImage.propTypes = {
    cascade: PropTypes.bool,
    children: PropTypes.node,
    className: PropTypes.string,
    hover: PropTypes.bool,
    overlay: PropTypes.string,
    src: PropTypes.string,
    tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
    top: PropTypes.bool,
    waves: PropTypes.bool,
    zoom: PropTypes.bool
};

CardImage.defaultProps = {
    tag: 'img',
    overlay: 'white-slight',
    waves: true,
    hover: false,
    cascade: false,
    zoom: false
};

export default CardImage;
export { CardImage as CDBCardImage };