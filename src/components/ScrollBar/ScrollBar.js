import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
import { Component } from "./ScrollBar.style";

const ScrollBar = (props) => {
  const { children, className, ...attrs } = props;
  const scrollBarClasses = classNames(className);
  let scrollBarComp = (
    <Component id="wrapper" className={scrollBarClasses}>
      <div class="scrollbar" id="style-default">
        <div class="force-overflow"></div>
      </div>

      <div class="scrollbar" id="style-1">
        <div class="force-overflow"></div>
      </div>

      <div class="scrollbar" id="style-2">
        <div class="force-overflow"></div>
      </div>

      <div class="scrollbar" id="style-3">
        <div class="force-overflow"></div>
      </div>

      <div class="scrollbar" id="style-4">
        <div class="force-overflow"></div>
      </div>

      <div class="scrollbar" id="style-5">
        <div class="force-overflow"></div>
      </div>

      <div class="scrollbar" id="style-6">
        <div class="force-overflow"></div>
      </div>

      <div class="scrollbar" id="style-7">
        <div class="force-overflow"></div>
      </div>

      <div class="scrollbar" id="style-8">
        <div class="force-overflow"></div>
      </div>

      <div class="scrollbar" id="style-9">
        <div class="force-overflow"></div>
      </div>

      <div class="scrollbar" id="style-10">
        <div class="force-overflow"></div>
      </div>

      <div class="scrollbar" id="style-11">
        <div class="force-overflow"></div>
      </div>

      <div class="scrollbar" id="style-13">
        <div class="force-overflow"></div>
      </div>

      <div class="scrollbar" id="style-14">
        <div class="force-overflow"></div>
      </div>

      <div class="scrollbar" id="style-15">
        <div class="force-overflow"></div>
      </div>
    </Component>
  );
  return scrollBarComp;
};

ScrollBar.defaultProps = {};

ScrollBar.propTypes = {
  className: PropTypes.string,
};

export default ScrollBar;

export { ScrollBar as CDBScrollBar };
