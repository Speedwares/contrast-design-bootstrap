import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { Component } from "./Breadcrumb.style";

const Breadcrumb = (props) => {
  const { className, color, light, uppercase, bold, ...attributes } = props;

  const classes = classNames("breadcrumb", className);

  let children;

  if (bold) {
    children = React.Children.map(props.children, (child) => {
      return React.cloneElement(child, {
        bold: true,
      });
    });
  } else {
    children = props.children;
  }
  let breadcrumbComponent = (
    <nav data-test="breadcrumb">
      <Component {...attributes} className={classes} colors={color}>
        {children}
      </Component>
    </nav>
  );
  return breadcrumbComponent;
};

Breadcrumb.propTypes = {
  bold: PropTypes.bool,
  children: PropTypes.node,
  className: PropTypes.string,
  color: PropTypes.string,
  light: PropTypes.bool,
  uppercase: PropTypes.bool,
};
Breadcrumb.defaultProps = {
  color: "primary",
};

export default Breadcrumb;
export { Breadcrumb as CDBBreadcrumb };
