import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import CDBCloseIcon from "../CloseIcon";
import CDBIcon from "../Icon";
import { Component } from "./Notification.style";

class Notification extends React.Component {
  state = {
    componentState: this.props.show ? "show" : "hide",
  };

  componentDidMount() {
    const { autohide } = this.props;
    if (autohide > 0) {
      this.hide(autohide);
    }
  }

  hide = (time = 0) => {
    if (typeof time === "object") {
      time = 0;
    }

    setTimeout(() => {
      this.setState({ componentState: "" }, () => {
        setTimeout(() => {
          this.setState({
            componentState: "hide",
          });
        }, 150);
      });
    }, time);
  };

  render() {
    const {
      tag,
      className,
      show,
      fade,
      message,
      bodyClassName,
      icon,
      iconClassName,
      title,
      titleClassName,
      text,
      closeClassName,
      ...attributes
    } = this.props;

    const { componentState } = this.state;

    const classes = classNames(
      "toast",
      fade && "fade",
      componentState,
      className
    );

    const headerClasses = classNames("toast-header", titleClassName);
    const iconClassNames = classNames("mr-2", iconClassName);
    const bodyClasses = classNames("toast-body", bodyClassName);
    const closeClasses = classNames("ml-2", "mb-1", closeClassName);

    return (
      <Component
        as={tag}
        data-test="notification"
        {...attributes}
        className={classes}
      >
        <div className={headerClasses}>
          <CDBIcon icon={icon} className={iconClassNames} size="lg" />
          <strong className="mr-auto">{title}</strong>
          <small>{text}</small>
          <CDBCloseIcon className={closeClasses} onClick={this.hide} />
        </div>
        <div className={bodyClasses}>{message}</div>
      </Component>
    );
  }
}

Notification.propTypes = {
  autohide: PropTypes.number,
  bodyClassName: PropTypes.string,
  bodyColor: PropTypes.string,
  className: PropTypes.string,
  closeClassName: PropTypes.string,
  fade: PropTypes.bool,
  iconClassName: PropTypes.string,
  message: PropTypes.string,
  show: PropTypes.bool,
  tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  text: PropTypes.string,
  title: PropTypes.string,
  titleClassName: PropTypes.string,
  titleColor: PropTypes.string,
};

Notification.defaultProps = {
  icon: "square",
  tag: "div",
  closeClassName: "text-dark",
};

export default Notification;
export { Notification as CDBNotification };
