import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { NavLink as NavLinkRouter } from 'react-router-dom';
import CDBLink from '../../Link';

const NavLink = props => {
    const { children, className, disabled, active, to, link, ...attributes } = props;
    const classes = classNames('nav-link', active && 'active', className);

    const Tag = link ? CDBLink : NavLinkRouter;

    return (
        <Tag
            data-test='nav-link'
            className={classes}
            to={to}
            {...attributes}
        >
            {children}
        </Tag>
    );
};

NavLink.propTypes = {
    active: PropTypes.bool,
    children: PropTypes.node,
    className: PropTypes.string,
    disabled: PropTypes.bool,
    link: PropTypes.bool,
    to: PropTypes.string
};

NavLink.defaultProps = {
    active: false,
    className: '',
    disabled: false,
    link: false
};

export default NavLink;
export { NavLink as CDBNavLink };