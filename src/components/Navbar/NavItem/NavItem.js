import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Component } from "./NavItem.style";
 

const NavItem = props => {
    const { children, className, active, text, tag, ...attributes } = props;

    const classes = classNames('nav-item', active && 'active', text && 'navbar-text', className);

    return (
        <Component as={tag} data-test='nav-item' {...attributes} className={classes}>
            {children}
        </Component>
    );
};

NavItem.propTypes = {
    active: PropTypes.bool,
    children: PropTypes.node,
    className: PropTypes.string,
    tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string])
};

NavItem.defaultProps = {
    tag: 'li'
};

export default NavItem;
export { NavItem as MDBNavItem };