import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Component } from "./NavbarNav.style";

const NavbarNav = props => {
    const { children, className, right, left, tag, ...attributes } = props;

    const classes = classNames(
        'navbar-nav',
        right ? 'ml-auto' : left ? 'mr-auto' : 'justify-content-around w-100',
        className
    );

    return (
        <Component as={tag} data-test='navbar-nav' {...attributes} className={classes}>
            {children}
        </Component>
    );
};

NavbarNav.propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
    left: PropTypes.bool,
    right: PropTypes.bool,
    tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string])
};

NavbarNav.defaultProps = {
    tag: 'ul'
};

export default NavbarNav;
export { NavbarNav as CDBNavbarNav };