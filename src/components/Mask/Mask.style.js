import styled from "styled-components";
import { variant, space, colorStyle } from "styled-system";


const patternGroup = {
  pattern1: "img/overlays/01.png",
  pattern2: "img/overlays/02.png",
  pattern3: "img/overlays/03.png",
  pattern4: "img/overlays/04.png",
};
const { pattern1, pattern2, pattern3, pattern4 } = patternGroup;
export const Component = styled("span")(
  space,
  colorStyle,
  {
    position: "absolute",
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    width: "100 %",
    height: "100 %",
    overflow: " hidden",
    backgroundAttachment: "fixed",
  },

  variant({
    prop: "pattern",

    variants: {
      pattern1: {
        backgroundImage: `url(${pattern1})`,
        backgroundAttachment: "fixed",
      },
      pattern2: {
        backgroundImage: `url(${pattern2})`,
        backgroundAttachment: "fixed",
      },
      pattern3: {
        backgroundImage: `url(${pattern3})`,
        backgroundAttachment: "fixed",
      },
      pattern4: {
        backgroundImage: `url(${pattern4})`,
        backgroundAttachment: "fixed",
      },
    },
  }),
  variant({
    prop: "overlay",
    variants: {},
  })
);
