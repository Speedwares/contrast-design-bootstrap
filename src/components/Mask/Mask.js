import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { Component } from "./Mask.style";

const Mask = (props) => {
  const { children, className, overlay, pattern, tag, ...attributes } = props;

  const maskClasses = classNames(className);
  let maskComponent = (
    <Component
      as={tag}
      data-test="mask"
      {...attributes}
      pattern={pattern}
      overlay={overlay}
      className={maskClasses}
    >
      {children}
    </Component>
  );
  return maskComponent;
};

Mask.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  overlay: PropTypes.string,
  pattern: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  tag: PropTypes.string,
};

Mask.defaultProps = {
  className: "",
  overlay: "",
  pattern: "",
  tag: "div",
};

export default Mask;
export { Mask as CDBMask };
