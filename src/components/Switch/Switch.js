import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { Component } from "./Switch.style";

const Switch = (props) => {
  const { checked, className } = props;
  const [isChecked, setIsChecked] = useState(checked);
  const switchClassName = classNames(className);
  const handleChange = () => {
    setIsChecked(!isChecked);
  };

  return (
    <Component className={switchClassName}>
      <div className="switch-container">
        <label>
          <input
            checked={isChecked}
            onChange={handleChange}
            className="switch"
            type="checkbox"
          />
          <div>
            <div></div>
          </div>
        </label>
      </div>
    </Component>
  );
};

Switch.propTypes = {
  checked: PropTypes.bool,
  tag: PropTypes.string,
};
Switch.defaultProps = {
  tag: "div",
  checked: false,
};

export default Switch;

export { Switch as CDBSwitch };
