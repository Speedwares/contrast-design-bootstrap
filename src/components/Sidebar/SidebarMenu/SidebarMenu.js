/* eslint-disable react/no-array-index-key */
import React, { forwardRef } from 'react';
import classNames from 'classnames';
import PropTypes from "prop-types";
import { MenuNav, MenuUl } from "./SidebarMenu.style"


const SidebarMenu = forwardRef(
  ({ children, className, iconShape, popperArrow, ...rest }, ref) => {
    const menuRef = ref ? ref : React.createRef();

    return (
      <MenuNav
        {...rest}
        ref={menuRef}
        className={classNames( className, {
          [`shaped ${iconShape}`]:
            ["square", "round", "circle"].indexOf(iconShape) >= 0
        })}
      >
        <MenuUl>
          {React.Children.map(children, child =>
            React.cloneElement(child, {
              firstchild: 1,
              popperarrow: popperArrow === true ? 1 : 0
            })
          )}
        </MenuUl>
      </MenuNav>
    );
  }
);


SidebarMenu.propTypes = {
    className: PropTypes.string,
    children: PropTypes.node,
    iconShape: PropTypes.oneOf(['square', 'round', 'circle']),
    popperArrow: PropTypes.bool
}

export default SidebarMenu;

export { SidebarMenu as CDBSidebarMenu };