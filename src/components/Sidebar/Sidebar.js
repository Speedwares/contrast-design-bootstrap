import React, { forwardRef, createContext, useEffect, useState } from 'react';
import PropTypes from "prop-types";
import classNames from 'classnames';
import {SidebarContainer, SidebarInner, SidebarLayout, SidebarOverlay} from "./Sidebar.style"


export const SidebarContext = createContext({
    collapsed: false,
    rtl: false,
    toggled: false,
});


const Sidebar = forwardRef((
    { children, className, width, collapsed, rtl, toggled, image, breakPoint, onToggle, ...rest },
    ref,
) => {
    const [sidebarState, setSidebarState] = useState({
        collapsed: collapsed === 'undefined' ? false : collapsed,
        rtl: rtl === 'undefined' ? false : rtl,
        toggled: toggled === 'undefined' ? false : toggled,
    });

    const sidebarRef = ref ? ref : React.createRef();


    const handleToggleSidebar = () => {
        const toggleValue = sidebarState.toggled;
        setSidebarState({ ...sidebarState, toggled: !toggleValue });
        if (onToggle) {
            onToggle(!toggleValue);
        }
    };

    useEffect(() => {
        setSidebarState({ ...sidebarState, collapsed, rtl, toggled });
    }, [collapsed, rtl, toggled]);

    return (
      <SidebarContext.Provider value={sidebarState}>
        <SidebarContainer
          {...rest}
          ref={sidebarRef}
          className={classNames("pro-sidebar", className, breakPoint, {
            collapsed,
            rtl,
            toggled
          })}
        >
          <SidebarInner>
            {/* {image ? <img src={image} alt="sidebar background" className="sidebar-bg" /> : null} */}
            <SidebarLayout>{children}</SidebarLayout>
          </SidebarInner>
          <SidebarOverlay
            onClick={handleToggleSidebar}
            onKeyPress={handleToggleSidebar}
            role="button"
            tabIndex={0}
            aria-label="overlay"
          />
        </SidebarContainer>
      </SidebarContext.Provider>
    );
});


Sidebar.propTypes = {
    collapsed: PropTypes.bool,
    rtl: PropTypes.bool,
    toggled: PropTypes.bool,
    width: PropTypes.oneOf([ PropTypes.string, PropTypes.number ]),
    image: PropTypes.string,
    className: PropTypes.string,
    children: PropTypes.node,
    breakPoint: PropTypes.oneOf([ "xl", "lg", "md", "sm", "xs" ]),
    onToggle: PropTypes.func
}


export default Sidebar;

export { Sidebar as CDBSidebar };