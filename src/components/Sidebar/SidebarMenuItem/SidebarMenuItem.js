import React, { forwardRef } from 'react';
import classNames from 'classnames';
import PropTypes from "prop-types";
import { InnerMenuItem } from "./SidebarMenuItem.style";



const SidebarMenuItem = forwardRef(
  (
    {
      children,
      className,
      icon,
      active,
      prefix,
      suffix,
      firstchild,
      popperarrow,
      ...rest
    },
    ref
  ) => {
    const menuItemRef = ref ? ref : React.createRef();

    return (
      <li
        {...rest}
        ref={menuItemRef}
        className={classNames(className, { active })}
      >
        <InnerMenuItem tabIndex={0} role="button">
          {icon ? (
            <span className="pro-icon-wrapper">
              <span className="pro-icon">{icon}</span>
            </span>
          ) : null}

          {prefix ? <span className="prefix-wrapper">{prefix}</span> : null}
          <span className="item-content">{children}</span>
          {suffix ? <span className="suffix-wrapper">{suffix}</span> : null}
        </InnerMenuItem>
      </li>
    );
  }
);

SidebarMenuItem.propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
    icon: PropTypes.node,
    active: PropTypes.bool,
    prefix: PropTypes.node,
    suffix: PropTypes.node,
    firstchild: PropTypes.number,
    popperarrow: PropTypes.number
}

export default SidebarMenuItem;

export { SidebarMenuItem as CDBSidebarMenuItem };