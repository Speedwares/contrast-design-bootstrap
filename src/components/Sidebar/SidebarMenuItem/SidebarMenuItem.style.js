import styled from "styled-components";

export const InnerMenuItem = styled.div`
         position: relative;
         display: flex;
         align-items: center;
         padding: 8px 35px 8px 20px;
         cursor: pointer;
         
         ::before {
           content: "";
           display: inline-block;
           width: 4px;
           min-width: 4px;
           height: 4px;
           border: 1px solid hsla(0, 0%, 100%, 0.05);
           border-radius: 50%;
           margin-right: 15px;
           position: relative;
           box-shadow: 1px 0 0 #adadad, 0 -1px 0 #adadad, 0 1px 0 #adadad,
             -1px 0 0 #adadad;
         }

         .item-content {
           flex-grow: 1;
           flex-shrink: 1;
           overflow: hidden;
           text-overflow: ellipsis;
           white-space: nowrap;
         }
       `;
