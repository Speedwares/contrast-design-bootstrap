import React, { useState, forwardRef, useRef, useEffect, useContext } from 'react';
import classNames from 'classnames';
import PropTypes from "prop-types";
import SlideDown from 'react-slidedown';
import { createPopper } from '@popperjs/core';
import ResizeObserver from 'resize-observer-polyfill';
import { SidebarContext } from '../Sidebar';
import {} from "./SidebarSubMenu.style"
import { InnerMenuItem, SubMenuContainer } from './SidebarSubMenu.style';


const SidebarSubMenu = forwardRef(
  (
    {
      children,
      icon,
      className,
      title,
      defaultOpen = false,
      open,
      prefix,
      suffix,
      firstchild,
      popperarrow,
      ...rest
    },
    ref
  ) => {
    let popperInstance;
    const { collapsed, rtl, toggled } = useContext(SidebarContext);
    const [closed, setClosed] = useState(!defaultOpen);
    const popperElRef = useRef(null);
    const referenceElement = useRef(null);
    const popperElement = useRef(null);

    const handleToggleSubMenu = () => {
      setClosed(!closed);
    };

    useEffect(() => {
      if (firstchild) {
        if (collapsed) {
          if (referenceElement.current && popperElement.current) {
            popperInstance = createPopper(
              referenceElement.current,
              popperElement.current,
              {
                placement: "right",
                strategy: "fixed",
                modifiers: [
                  {
                    name: "computeStyles",
                    options: {
                      adaptive: false
                    }
                  }
                ]
              }
            );
          }

          if (popperElRef.current) {
            const ro = new ResizeObserver(() => {
              if (popperInstance) {
                popperInstance.update();
              }
            });

            ro.observe(popperElRef.current);
            ro.observe(referenceElement.current);
          }

          setTimeout(() => {
            if (popperInstance) {
              popperInstance.update();
            }
          }, 300);
        }
      }

      return () => {
        if (popperInstance) {
          popperInstance.destroy();
          popperInstance = null;
        }
      };
    }, [collapsed, rtl, toggled]);

    const subMenuRef = ref ? ref : React.createRef();

    return (
      <SubMenuContainer
        ref={subMenuRef}
        className={classNames("pro-menu-item pro-sub-menu", className, {
          open: typeof open === "undefined" ? !closed : open
        })}
      >
        <InnerMenuItem
          {...rest}
          ref={referenceElement}
          onClick={handleToggleSubMenu}
          onKeyPress={handleToggleSubMenu}
          role="button"
          tabIndex={0}
        >
          {icon ? (
            <span className="pro-icon-wrapper">
              <span className="pro-icon">{icon}</span>
            </span>
          ) : null}
          {prefix ? <span className="prefix-wrapper">{prefix}</span> : null}
          <span className="pro-item-content">{title}</span>
          {suffix ? <span className="suffix-wrapper">{suffix}</span> : null}
          <span className="pro-arrow-wrapper">
            <span className="pro-arrow" />
          </span>
        </InnerMenuItem>

        {firstchild && collapsed ? (
          <div
            ref={popperElement}
            className={classNames("pro-inner-list-item popper-element", {
              "has-arrow": popperarrow
            })}
          >
            <div className="popper-inner" ref={popperElRef}>
              <ul>{children}</ul>
            </div>
            {popperarrow ? (
              <div className="popper-arrow" data-popper-arrow />
            ) : null}
          </div>
        ) : (
          <SlideDown
            closed={typeof open === "undefined" ? closed : !open}
            className="inner-list-item"
          >
            <div>
              <ul>{children}</ul>
            </div>
          </SlideDown>
        )}
      </SubMenuContainer>
    );
  }
);


SidebarSubMenu.propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
    icon: PropTypes.node,
    title: PropTypes.node,
    defaultOpen: PropTypes.bool,
    open: PropTypes.bool,
    prefix: PropTypes.node,
    suffix: PropTypes.node,
    firstchild: PropTypes.number,
    popperarrow: PropTypes.number
}


export default SidebarSubMenu;

export { SidebarSubMenu as CDBSidebarSubMenu };