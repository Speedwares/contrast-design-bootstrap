import styled from "styled-components";

export const InnerMenuItem = styled.div`
         position: relative;
         display: flex;
         align-items: center;
         padding: 8px 35px 8px 20px;
         cursor: pointer;

         .item-content {
           flex-grow: 1;
           flex-shrink: 1;
           overflow: hidden;
           text-overflow: ellipsis;
           white-space: nowrap;
         }
         .inner-list-item {
           position: relative;
           background-color: hsla(0, 0%, 100%, 0.05);
         }
         .react-slidedown {
           height: 0;
           transition-property: none;
           transition-duration: 0.2s;
           transition-timing-function: ease-in-out;
         }

         .react-slidedown.transitioning {
           overflow-y: hidden;
         }

         .react-slidedown.closed {
           display: none;
         }
       `;


export const SubMenuContainer = styled.li`
  .react-slidedown {
    height: 0;
    transition-property: none;
    transition-duration: 0.2s;
    transition-timing-function: ease-in-out;
  }

  .react-slidedown.transitioning {
    overflow-y: hidden;
  }

  .react-slidedown.closed {
    display: none;
  }
`;

