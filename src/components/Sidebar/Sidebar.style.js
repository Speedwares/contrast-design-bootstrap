import styled from "styled-components";


export const SidebarContainer = styled.div`
    color: #adadad;
    height: 100%;
    width: 270px;
    min-width: 270px;
    text-align: left;
    transition: width, left, right, 0.3s;
    position: relative;
    z-index: 1009;
`;

export const SidebarInner = styled.div`
    background: #1d1d1d;
    height: 100%;
    position: relative;
    z-index: 101;
`;

export const SidebarLayout = styled.div`
    height: 100%;
    overflow-y: auto;
    overflow-x: hidden;
    position: relative;
    display: flex;
    flex-direction: column;
    z-index: 101;
`;


export const SidebarOverlay = styled.div`
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background-color: rgba(0, 0, 0, 0.3);
    z-index: 100;
    display: none;
`;

