import React, { forwardRef } from 'react';
import classNames from 'classnames';
import PropTypes from "prop-types";
import { HeaderContainer } from "./SidebarHeader.style"


const SidebarHeader = forwardRef(({ children, className, ...rest }, ref) => {
  const sidebarHeaderRef = ref ? ref : React.createRef();

  return (
    <HeaderContainer
      {...rest}
      ref={sidebarHeaderRef}
      className={classNames(className)}
    >
      {children}
    </HeaderContainer>
  );
});

SidebarHeader.propTypes = {
    className: PropTypes.string,
    children: PropTypes.node
}

export default SidebarHeader;

export { SidebarHeader as CDBSidebarHeader };