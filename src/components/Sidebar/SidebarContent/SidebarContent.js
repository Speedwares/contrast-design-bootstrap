import React, { forwardRef } from 'react';
import classNames from 'classnames';
import PropTypes from "prop-types";
import { ContentContainer } from "./SidebarContent.style"


const SidebarContent = forwardRef(({ children, className, ...rest }, ref) => {
  const sidebarContentRef = ref ? ref : React.createRef();

  return (
    <ContentContainer
      {...rest}
      ref={sidebarContentRef}
      className={classNames("pro-sidebar-content", className)}
    >
      {children}
    </ContentContainer>
  );
});

SidebarContent.propTypes = {
    className: PropTypes.string,
    children: PropTypes.node
}


export default SidebarContent;
export { SidebarContent as CDBSidebarContent };