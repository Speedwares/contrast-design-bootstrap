import styled from "styled-components";

export const ContentContainer = styled.div`
    flex-grow: 1;
`;
