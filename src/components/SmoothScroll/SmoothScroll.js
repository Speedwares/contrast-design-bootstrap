import React from "react";
import PropTypes from "prop-types";

import { Link, animateScroll as scroll } from "react-scroll";

const SmoothScroll = (props) => {
  const { children, ...attrs } = props;
  let smoothScrollComp = (
    <Link {...attrs}>
      {children}
    </Link>
  );
  return smoothScrollComp;
};

SmoothScroll.defaultProps = {
  offset: -70,
  duration: 500,
  smooth: true,
  spy: true,
};

SmoothScroll.propTypes = {
  className: PropTypes.string,
  to: PropTypes.string,
  smooth: PropTypes.bool,
  spy: PropTypes.bool,
  offset: PropTypes.number,
  duration: PropTypes.number,
  activeClass: PropTypes.string,
};

export default SmoothScroll;

export { SmoothScroll as CDBSmoothScroll };
