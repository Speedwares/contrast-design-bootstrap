export const radioGroupStyle = {
  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
};
export const radioBtnContainerStyle = {
  display: "flex",
  alignItems: "center",
  cursor: "pointer",
  fontSize: "1.5rem",
  padding: "1rem",
};
export const radioBtnInputStyle = {
  position: "absolute",
  opacity: 0,
  cursor: "pointer",
  height: 0,
  width: 0,
};
export const radioBtnUncheckedStyle = {
  border: `.2rem solid ${fill}`,
};
export const radioBtnCheckedStyle = {
  backgroundColor: fill,
  border: `.2rem solid ${chosenFill}`,
  boxShadow: `0 0 0 .2rem ${fill}`,
};
export const radioBtnHoveredStyle = {
  boxShadow: `0 0 0 .4rem ${fill}75`,
  border: `.2rem solid ${fill}`,
};
export const radioBtnMousedStyle = {
  border: `.2rem solid ${fill}75`,
};
export const radioBtnStyle = {
  display: "inline-block",
  height: ".9rem",
  width: ".9rem",
  borderRadius: "50%",
  marginRight: "1rem",
  transition: ".15s ease-in-out",
};
