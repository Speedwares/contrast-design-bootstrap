import React from "react";
import PropTypes from "prop-types";

class Radio extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      chosen: false,
      hovered: false,
      moused: false,
    };
  }
  handleClick = (e) => {
    e.preventDefault();

    return this.setState((state) => ({
      ...state,
      moused: false,
      chosen: true,
    }));
  };
  handleMouseDown = (e) => {
    e.preventDefault();

    return this.setState((state) => ({
      ...state,
      moused: true,
    }));
  };
  handleMouseEnter = (e) => {
    e.preventDefault();

    return this.setState((state) => ({
      ...state,
      hovered: true,
    }));
  };
  handleMouseLeave = (e) => {
    e.preventDefault();

    return this.setState((state) => ({
      ...state,
      hovered: false,
    }));
  };
  render() {
    const { fill, chosenFill, name, value } = this.props;
    const radioGroupStyle = {
      display: "flex",
      flexDirection: "column",
      justifyContent: "center",
    };
    const radioBtnContainerStyle = {
      display: "flex",
      alignItems: "center",
      cursor: "pointer",
      fontSize: "1rem",
      padding: "1rem",
    };
    const radioBtnInputStyle = {
      position: "absolute",
      opacity: 0,
      cursor: "pointer",
      height: 0,
      width: 0,
    };
    const radioBtnUncheckedStyle = {
      border: `.2rem solid ${fill}`,
    };
    const radioBtnCheckedStyle = {
      backgroundColor: fill,
      border: `.2rem solid ${chosenFill}`,
      boxShadow: `0 0 0 .2rem ${fill}`,
    };
    const radioBtnHoveredStyle = {
      boxShadow: `0 0 0 .4rem ${fill}75`,
      border: `.2rem solid ${fill}`,
    };
    const radioBtnMousedStyle = {
      border: `.2rem solid ${fill}75`,
    };
    const radioBtnStyle = {
      display: "inline-block",
      height: ".9rem",
      width: ".9rem",
      borderRadius: "50%",
      marginRight: "1rem",
      transition: ".15s ease-in-out",
    };

    return (
      <div id="radio-group" style={radioGroupStyle}>
        <label
          onClick={(e) => this.handleClick(e)}
          onMouseEnter={(e) => this.handleMouseEnter(e)}
          onMouseLeave={(e) => this.handleMouseLeave(e)}
          onMouseDown={(e) => this.handleMouseDown(e)}
          style={radioBtnContainerStyle}
        >
          <input
            type="radio"
            value={value}
            name={name}
            checked={this.state.chosen}
            style={radioBtnInputStyle}
          />
          <div
            style={{
              ...radioBtnStyle,
              ...(this.state.chosen
                ? radioBtnCheckedStyle
                : this.state.moused
                ? radioBtnMousedStyle
                : this.state.hovered
                ? radioBtnHoveredStyle
                : radioBtnUncheckedStyle),
            }}
          />
          {value}
        </label>
      </div>
    );
  }
}
Radio.defaultProps = {
  colorFill: "#455ff5",
  fill: "#455ff5",
};

Radio.propTypes = {
  className: PropTypes.string,
  name: PropTypes.string,
  value: PropTypes.string,
};

export default Radio;

export { Radio as CDBRadio };
