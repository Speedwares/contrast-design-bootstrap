import React, { useState } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import RangeSlider from "react-bootstrap-range-slider";

const Slider = (props) => {
  const { value, setValue, className, ...attrs } = props;
  const sliderClasses = classNames("slider", className);
  let sliderComponent = (
    <RangeSlider
      value={value}
      onChange={(changeEvent) => setValue(changeEvent.target.value)}
      className={sliderClasses}
      {...attrs}
    />
  );
  return sliderComponent;
};
Slider.propTypes = {
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  onChange: PropTypes.func,
  onAfterChange: PropTypes.func,
  min: PropTypes.number,
  max: PropTypes.number,
  step: PropTypes.number,
  disabled: PropTypes.bool,
  size: PropTypes.oneOf(["sm", "lg"]),
  variant: PropTypes.oneOf([
    "primary",
    "secondary",
    "success",
    "danger",
    "warning",
    "info",
    "dark",
    "light",
  ]),
  inputProps: PropTypes.object,
  tooltip: PropTypes.oneOf(["auto", "on", "off"]),
  tooltipPlacement: PropTypes.oneOf(["top", "bottom"]),
  tooltipLabel: PropTypes.func,
  tooltipStyle: PropTypes.object,
  tooltipProps: PropTypes.object,
  className: PropTypes.string,
  ref: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.shape({ current: PropTypes.instanceOf(Element) }),
  ]),
  bsPrefix: PropTypes.string,
};
Slider.defaultProps = {
  min: 0,
  max: 100,
  step: 1,
};

export default Slider;
export { Slider as CDBSlider };
