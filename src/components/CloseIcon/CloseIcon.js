import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

export const CloseIcon = (props) => {
  const { onClick, className, ariaLabel, ...attrs } = props;
  const onClickHandler = (e) => {
    onClick && onClick(e);
  };
  const closeIconClasses = classNames("close", className);
  let closeIconComponent = (
    <button
      data-test="close-button"
      type="button"
      {...attrs}
      className={closeIconClasses}
      onClick={onClickHandler}
      aria-label={ariaLabel}
    >
      <span aria-hidden="true">×</span>
    </button>
  );
  return closeIconComponent;
};

CloseIcon.defaultProps = {
  ariaLabel: "Close",
};

CloseIcon.propTypes = {
  ariaLabel: PropTypes.string,
  className: PropTypes.string,
  onClick: PropTypes.func,
};

export default CloseIcon;
export { CloseIcon as CDBCloseIcon };
