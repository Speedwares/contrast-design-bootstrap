import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import AccordionItem from "./AccordionItem";
import { Component } from "./Accordion.style";

const Accordion = (props) => {
  const {
    data,
    accordionClass,
    accordionHeaderClass,
    accordionBodyClass,
    hideIcon,
  } = props;
  const accordionClassName = classNames("wrapper", accordionClass);
  return (
    <Component className={accordionClassName} hideIcon={hideIcon}>
      <ul className="accordion-list">
        {data.map((data, key) => {
          return (
            <li className="accordion-list__item" key={key}>
              <AccordionItem
                {...data}
                accordionHeaderClass={accordionHeaderClass}
                accordionBodyClass={accordionBodyClass}
              />
            </li>
          );
        })}
      </ul>
    </Component>
  );
};

AccordionItem.propTypes = {
  accordionClass: PropTypes.string,
  accordionHeaderClass: PropTypes.string,
  accordionBodyClass: PropTypes.string,
  hideIcon: PropTypes.bool,
  tag: PropTypes.string,
};
AccordionItem.defaultProps = {
  tag: "div",
};

export default Accordion;

export { Accordion as CDBAccordion };
