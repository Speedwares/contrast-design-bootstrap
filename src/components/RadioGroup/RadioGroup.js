import React from "react";
import PropTypes from "prop-types";

class RadioGroup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      answers: this.props.answers.map((answer) => ({
        answer,
        chosen: false,
        hovered: false,
        moused: false,
      })),
    };
  }
  handleClick = (e, index) => {
    e.preventDefault();
    return this.setState((state) => ({
      answers: state.answers.map(({ ...rest }, sIndex) => ({
        ...rest,
        moused: false,
        chosen: index === sIndex ? true : false,
      })),
    }));
  };
  handleMouseDown = (e, index) => {
    e.preventDefault();

    return this.setState((state) => ({
      answers: state.answers.map(({ ...rest }, sIndex) => ({
        ...rest,
        moused: index === sIndex ? true : false,
      })),
    }));
  };
  handleMouseEnter = (e, index) => {
    e.preventDefault();

    return this.setState((state) => ({
      answers: state.answers.map(({ ...rest }, sIndex) => ({
        ...rest,
        hovered: index === sIndex ? true : false,
      })),
    }));
  };
  handleMouseLeave = (e, index) => {
    e.preventDefault();

    return this.setState((state) => ({
      answers: state.answers.map(({ hovered, ...rest }, sIndex) => ({
        ...rest,
        hovered: index === sIndex ? false : hovered,
      })),
    }));
  };
  render() {
    const { fill, chosenFill } = this.props;
    const radioGroupStyle = {
      display: "flex",
      flexDirection: "column",
      justifyContent: "center",
    };
    const radioBtnContainerStyle = {
      display: "flex",
      alignItems: "center",
      cursor: "pointer",
      fontSize: "1rem",
      padding: "1rem",
    };
    const radioBtnInputStyle = {
      position: "absolute",
      opacity: 0,
      cursor: "pointer",
      height: 0,
      width: 0,
    };
    const radioBtnUncheckedStyle = {
      border: `.2rem solid ${fill}`,
    };
    const radioBtnCheckedStyle = {
      backgroundColor: fill,
      border: `.2rem solid ${chosenFill}`,
      boxShadow: `0 0 0 .2rem ${fill}`,
    };
    const radioBtnHoveredStyle = {
      boxShadow: `0 0 0 .4rem ${fill}75`,
      border: `.2rem solid ${fill}`,
    };
    const radioBtnMousedStyle = {
      border: `.2rem solid ${fill}75`,
    };
    const radioBtnStyle = {
      display: "inline-block",
      height: ".9rem",
      width: ".9rem",
      borderRadius: "50%",
      marginRight: "1rem",
      transition: ".15s ease-in-out",
    };

    return (
      <div id="radio-group" style={radioGroupStyle}>
        {this.state.answers.map(
          ({ answer, hovered, chosen, moused }, index) => {
            return (
              <label
                onClick={(e) => this.handleClick(e, index)}
                onMouseEnter={(e) => this.handleMouseEnter(e, index)}
                onMouseLeave={(e) => this.handleMouseLeave(e, index)}
                onMouseDown={(e) => this.handleMouseDown(e, index)}
                style={radioBtnContainerStyle}
              >
                <input
                  type="radio"
                  value={answer}
                  name={answer}
                  checked={chosen}
                  style={radioBtnInputStyle}
                />
                <div
                  style={{
                    ...radioBtnStyle,
                    ...(chosen
                      ? radioBtnCheckedStyle
                      : moused
                      ? radioBtnMousedStyle
                      : hovered
                      ? radioBtnHoveredStyle
                      : radioBtnUncheckedStyle),
                  }}
                />
                {answer}
              </label>
            );
          }
        )}
      </div>
    );
  }
}

export default RadioGroup;

export { RadioGroup as CDBRadioGroup };
