import React, { Component } from "react";
import PropTypes from "prop-types";
import CDBInput from "../Input";
import { StyledComponent } from "./Autocomplete.style";

class Autocomplete extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeSuggestion: 0,
      filteredSuggestions: [],
      showSuggestions: false,
      userInput: "",
    };
  }
  onChange = (e) => {
    const { suggestions, getValue } = this.props;
    const userInput = e.currentTarget.value;
    if (suggestions) {
      const filteredSuggestions = suggestions.filter(
        (suggestion) =>
          suggestion.toLowerCase().indexOf(userInput.toLowerCase()) > -1
      );

      this.setState({
        activeSuggestion: 0,
        filteredSuggestions,
        showSuggestions: true,
        userInput: e.currentTarget.value,
      });
      getValue && getValue(e.currentTarget.value);
    } else {
      return null;
    }
  };
  onClick = (e) => {
    const { getValue } = this.props;

    this.setState({
      activeSuggestion: 0,
      filteredSuggestions: [],
      showSuggestions: false,
      userInput: e.currentTarget.innerText,
    });
    getValue && getValue(e.currentTarget.innerText);
  };
  onKeyDown = (e) => {
    const { activeSuggestion, filteredSuggestions } = this.state;

    if (e.keyCode === 13) {
      this.setState({
        activeSuggestion: 0,
        showSuggestions: false,
        userInput: filteredSuggestions[activeSuggestion],
      });
    } else if (e.keyCode === 38) {
      if (activeSuggestion === 0) {
        return;
      }

      this.setState({ activeSuggestion: activeSuggestion - 1 });
    } else if (e.keyCode === 40) {
      if (activeSuggestion - 1 === filteredSuggestions.length) {
        return;
      }

      this.setState({ activeSuggestion: activeSuggestion + 1 });
    }
  };
  render() {
    const { label } = this.props;
    const { filteredSuggestions, showSuggestions, userInput } = this.state;
    let suggestionsListComponent;
    if (showSuggestions && userInput) {
      if (filteredSuggestions.length) {
        suggestionsListComponent = (
          <ul className="suggestions">
            {filteredSuggestions.map((suggestion, index) => {
              return (
                <li key={suggestion} onClick={this.onClick}>
                  {suggestion}
                </li>
              );
            })}
          </ul>
        );
      } else {
        suggestionsListComponent = null;
      }
    }
    return (
      <StyledComponent>
        <CDBInput
          type="text"
          onChange={this.onChange}
          onKeyDown={this.onKeyDown}
          value={userInput}
          label={label}
        />
        {suggestionsListComponent}
      </StyledComponent>
    );
  }
}
Autocomplete.propTypes = {
  suggestion: PropTypes.array,
  getValue: PropTypes.func,
  label: PropTypes.string,
};
Autocomplete.defaultProps = {
  suggestion: [],
};
export default Autocomplete;
export { Autocomplete as CDBAutocomplete };
