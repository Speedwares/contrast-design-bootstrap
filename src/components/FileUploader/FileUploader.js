import React, { useRef, useEffect } from "react";
import PropTypes from "prop-types";
import { Component } from "./FileUploader.style";

const FileUploader = (props) => {
  const fileSelect = useRef(null); //.file-upload
  const fileDrag = useRef(null); // .file-drag
  const submitButton = useRef(null); // .submit-button
  const messageBar = useRef(null); // .messages
  const start = useRef(null); // #start
  const response = useRef(null); // #response
  const notImage = useRef(null); // #not-image
  const fileImage = useRef(null); // #file-image
  const fileUploadForm = useRef(null); // #file-upload-form
  const pBar = useRef(null); // #file-progress

  useEffect(() => {
    if (window.File && window.FileList && window.FileReader) {
      fileUpload();
    } else {
      fileDrag.current.style.display = "none";
    }
  });
  const fileUpload = () => {
    console.log("Upload Initialized");
    fileSelect.current.addEventListener("change", fileSelectHandler, false);
    var xhr = new XMLHttpRequest();
    if (xhr.upload) {
      fileDrag.current.addEventListener("dragover", fileDragHover, false);
      fileDrag.current.addEventListener("dragleave", fileDragHover, false);
      fileDrag.current.addEventListener("drop", fileSelectHandler, false);
    }
  };
  const fileDragHover = (e) => {
    e.stopPropagation();
    e.preventDefault();
    fileDrag.current.className =
      e.type === "dragover" ? "hover" : "modal-body file-upload";
  };

  const fileSelectHandler = (e) => {
    var files = e.target.files || e.dataTransfer.files;
    fileDragHover(e);
    for (var i = 0, f; (f = files[i]); i++) {
      parseFile(f);
      uploadFile(f);
    }
  };
  const output = (msg) => {
    messageBar.current.innerHTML = msg;
  };

  const parseFile = (file) => {
    console.log(file.name);
    output("<strong>" + encodeURI(file.name) + "</strong>");
    var imageName = file.name;
    var isGood = /\.(?=gif|jpg|png|jpeg)/gi.test(imageName);
    if (isGood) {
      start.current.classList.add("hidden");
      response.current.classList.remove("hidden");
      notImage.current.classList.add("hidden");
      fileImage.current.classList.remove("hidden");
      fileImage.current.src = URL.createObjectURL(file);
    } else {
      fileImage.current.classList.add("hidden");
      notImage.current.classList.remove("hidden");
      start.current.classList.remove("hidden");
      response.current.classList.add("hidden");
      fileUploadForm.current.reset();
    }
  };

  const setProgressMaxValue = (e) => {
    if (e.lengthComputable) {
      pBar.current.max = e.total;
    }
  };
  const updateFileProgress = (e) => {
    if (e.lengthComputable) {
      pBar.current.value = e.loaded;
    }
  };
  const uploadFile = (file) => {
    var xhr = new XMLHttpRequest(),
      fileSizeLimit = 1024;
    if (xhr.upload) {
      if (file.size <= fileSizeLimit * 1024 * 1024) {
        pBar.current.style.display = "inline";
        xhr.upload.addEventListener("loadstart", setProgressMaxValue, false);
        xhr.upload.addEventListener("progress", updateFileProgress, false);
        xhr.onreadystatechange = function (e) {
          if (xhr.readyState == 4) {
          }
        };
        xhr.open("POST", fileUploadForm.current.action, true);
        xhr.setRequestHeader("X-File-Name", file.name);
        xhr.setRequestHeader("X-File-Size", file.size);
        xhr.setRequestHeader("Content-Type", "multipart/form-data");
        xhr.send(file);
      } else {
        output("Please upload a smaller file (< " + fileSizeLimit + " MB).");
      }
    }
  };

  let fileUploadComponent = (
    <Component>
      <form className="uploader" ref={fileUploadForm}>
        <input
          id="file-upload"
          ref={fileSelect}
          type="file"
          name="fileUpload"
          accept="image/*"
        />
        <label for="file-upload" id="file-drag" ref={fileDrag}>
          <img
            id="file-image"
            ref={fileImage}
            src="#"
            alt="Preview"
            class="hidden"
          />
          <div id="start" ref={start}>
            <i className="fa fa-download" aria-hidden="true"></i>
            <div>Select a file or drag here</div>
            <div id="notimage" ref={notImage} className="hidden">
              Please select an image
            </div>
            <span
              id="file-upload-btn"
              ref={submitButton}
              className="btn btn-primary"
            >
              Select a file
            </span>
          </div>
          <div id="response" ref={response} className="hidden">
            <div id="messages" ref={messageBar}></div>
            <progress
              className="progress"
              ref={pBar}
              id="file-progress"
              value="0"
            >
              <span>0</span>%
            </progress>
          </div>
        </label>
      </form>
    </Component>
  );
  return fileUploadComponent;
};

export default FileUploader;

export { FileUploader as CDBFileUploader };
