import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
import { Link as LinkRouter } from "react-router-dom";

const Link = (props) => {
  const {
    active,
    children,
    className,
    disabled,
    link,
    onMouseUp,
    onTouchStart,
    to,
    ...attributes
  } = props;
  const classes = classNames(
    "nav-link",
    disabled && "disabled",
    active && "active",
    className
  );

  let linkComponent = (
    <LinkRouter
      data-test="link-router"
      className={classes}
      onMouseUp={onMouseUp}
      onTouchStart={onTouchStart}
      to={to}
      {...attributes}
    >
      {children}
    </LinkRouter>
  );

  return linkComponent;
};

Link.propTypes = {
  active: PropTypes.bool,
  children: PropTypes.node,
  className: PropTypes.string,
  disabled: PropTypes.bool,
  to: PropTypes.string,
  onMouseUp: PropTypes.func,
  onTouchStart: PropTypes.func,
};

Link.defaultProps = {
  active: false,
  className: "",
  disabled: false,
};

export default Link;
export { Link as CDBLink };
