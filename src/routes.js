import React, { Fragment } from "react";
import { BrowserRouter, Route } from "react-router-dom";
import { Layout } from "./Layout";
import { Alert } from "./containers/Alert";
import { DropDown } from "./containers/DropDown";
import { Iframe } from "./containers/Iframe";
import { Badge } from "./containers/Badge";
import { Spinner } from "./containers/Spinner";
import { Button } from "./containers/Button";
import { ButtonGroup } from "./containers/ButtonGroup";
import { ButtonToolbar } from "./containers/ButtonToolbar";
import { Icon } from "./containers/Icon";
import { ListGroup } from "./containers/ListGroup";
import { Pagination } from "./containers/Pagination";
import { Progress } from "./containers/Progress";
import { Box } from "./containers/Box";
import { Animation } from "./containers/Animation";
import { Notification } from "./containers/Notification";
import { Table } from "./containers/Table";
import { Carousel } from "./containers/Carousel";
import { Rating } from "./containers/Rating";
import { Modal } from "./containers/Modal";
import { Card } from "./containers/Card";
import { Mask } from "./containers/Mask";
import { Select } from "./containers/Select";
import DataTables from "./containers/DataTables";
import Collapse from "./containers/Collapse";
import { NavbarExample } from "./containers/Navbar";
import { SidebarContainer } from "./containers/Sidebar";
import Tabs from "./containers/Tabs";
import { DatePickerPage } from "./containers/DatePicker";
import TimePickerPage from "./containers/TimePicker";
import { Accordion } from "./containers/Accordion";
import { Switch } from "./containers/Switch";
import { Input } from "./containers/Input";
import { InputGroup } from "./containers/InputGroup";
import { Radio } from "./containers/Radio";
import { FileUploader } from "./containers/FileUploader";
import { Slider } from "./containers/Slider";
import ChartsPage from "./containers/Chart";
import { SmoothScroll } from "./containers/SmoothScroll";
import { ScrollBar } from "./containers/ScrollBar";
import { Autocomplete } from "./containers/Autocomplete";
import Stepper from "./containers/Stepper";

const Routes = () => {
  return (
    <Fragment>
      <BrowserRouter>
        <Layout>
          <Route exact path="/" render={() => <Fragment />} />
          <Route path="/alert" component={Alert} />
          <Route path="/dropdown" component={DropDown} />
          <Route path="/iframe" component={Iframe} />
          <Route path="/badge" component={Badge} />
          <Route path="/spinner" component={Spinner} />
          <Route path="/button" component={Button} />
          <Route path="/buttongroup" component={ButtonGroup} />
          <Route path="/buttontoolbar" component={ButtonToolbar} />
          <Route path="/icon" component={Icon} />
          <Route path="/listgroup" component={ListGroup} />
          <Route path="/pagination" component={Pagination} />
          <Route path="/progress" component={Progress} />
          <Route path="/box" component={Box} />
          <Route path="/animation" component={Animation} />
          <Route path="/notification" component={Notification} />
          <Route path="/table" component={Table} />
          <Route path="/carousel" component={Carousel} />
          <Route path="/rating" component={Rating} />
          <Route path="/modal" component={Modal} />
          <Route path="/card" component={Card} />
          <Route path="/mask" component={Mask} />
          <Route path="/select" component={Select} />
          <Route path="/datatable" component={DataTables} />
          <Route path="/collapse" component={Collapse} />
          <Route path="/navbar" component={NavbarExample} />
          <Route path="/sidebar" component={SidebarContainer} />
          <Route path="/tabs" component={Tabs} />
          <Route path="/date-picker" component={DatePickerPage} />
          <Route path="/time-picker" component={TimePickerPage} />
          <Route path="/accordion" component={Accordion} />
          <Route path="/switch" component={Switch} />
          <Route path="/input" component={Input} />
          <Route path="/input-group" component={InputGroup} />
          <Route path="/radio" component={Radio} />
          <Route path="/file-uploader" component={FileUploader} />
          <Route path="/slider" component={Slider} />
          <Route path="/chart" component={ChartsPage} />
          <Route path="/smooth-scroll" component={SmoothScroll} />
          <Route path="/scrollbar" component={ScrollBar} />
          <Route path="/autocomplete" component={Autocomplete} />
          <Route path="/stepper" component={Stepper} />
        </Layout>
      </BrowserRouter>
    </Fragment>
  );
};

export default Routes;
