import colors from "./colors";

export const theme = {
  colors,
  colorStyles: {
    primary: {
      color: colors.white,
      borderColor: colors.primary,
      backgroundColor: colors.primary,
      "&:hover": {
        color: colors.white,
        backgroundColor: colors.primaryHover,
      },
    },
    secondary: {
      color: colors.white,
      borderColor: colors.secondary,
      backgroundColor: colors.secondary,
      "&:hover": {
        color: colors.white,
        backgroundColor: colors.secondaryHover,
      },
    },
    success: {
      color: colors.white,
      borderColor: colors.success,
      backgroundColor: colors.success,
      "&:hover": {
        color: colors.white,
        backgroundColor: colors.successHover,
      },
    },
    danger: {
      color: colors.white,
      borderColor: colors.danger,
      backgroundColor: colors.danger,
      "&:hover": {
        color: colors.white,
        backgroundColor: colors.dangerHover,
      },
    },
    warning: {
      color: colors.warningDark,
      borderColor: colors.warning,
      backgroundColor: colors.warning,
      "&:hover": {
        color: colors.warningDark,
        backgroundColor: colors.warningHover,
      },
    },
    dark: {
      color: colors.white,
      borderColor: colors.dark,
      backgroundColor: colors.dark,
      "&:hover": {
        color: colors.white,
        backgroundColor: colors.darkHover,
      },
    },
    white: {
      color: colors.dark,
      borderColor: colors.white,
      backgroundColor: colors.white,
      "&:hover": {
        color: colors.dark,
        backgroundColor: colors.whiteHover,
      },
    },
    info: {
      color: colors.white,
      borderColor: colors.info,
      backgroundColor: colors.info,
      "&:hover": {
        color: colors.white,
        backgroundColor: colors.infoHover,
      },
    },
  },
  buttonStyle: {
    primary: {
      color: colors.white,
      borderColor: colors.primary,
      backgroundColor: colors.primary,
    },
    secondary: {
      color: colors.white,
      borderColor: colors.secondary,
      backgroundColor: colors.secondary,
    },
    success: {
      color: colors.white,
      borderColor: colors.success,
      backgroundColor: colors.success,
    },
    danger: {
      color: colors.white,
      borderColor: colors.danger,
      backgroundColor: colors.danger,
    },
    warning: {
      color: colors.warningDark,
      borderColor: colors.warning,
      backgroundColor: colors.warning,
    },
  },
};
