import React from "react";
import { StyledLayout } from "./Layout.style";

export const Layout = ({ children }) => {
  return <StyledLayout>{children}</StyledLayout>;
};
