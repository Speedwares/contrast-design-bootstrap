import React, { Fragment } from "react";
import CDBBtn from "../components/Button";
import CDBCard from "../components/Card";
import CDBCardImage from "../components/Card/CardImage";
import CDBCardBody from "../components/Card/CardBody";
import CDBCardTitle from "../components/Card/CardTitle";
import CDBCardText from "../components/Card/CardText";

export const Card = () => {
    return (
        <Fragment>
            <CDBCard style={{ width: "22rem" }}>
                <CDBCardImage className="img-fluid" src="https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__340.jpg" />
                <CDBCardBody>
                    <CDBCardTitle>Card title</CDBCardTitle>
                    <CDBCardText>
                        Some quick example text to build on the card title and make
                        up the bulk of the card&apos;s content.
                    </CDBCardText>
                    <CDBBtn href="#">CDBBtn</CDBBtn>
                </CDBCardBody>
            </CDBCard>
        </Fragment>
    );
};