import React, { Fragment } from "react";
import CDBAccordion from "../components/Accordion";
import CDBContainer from "../components/Container";

const Paragraph = () => {
  return (
    <CDBContainer>
      Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
      richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard
      dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf
      moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla
      assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore
      wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur
      butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim
      aesthetic synth nesciunt you probably haven't heard of them accusamus
      labore sustainable VHS.
    </CDBContainer>
  );
};

export const Accordion = () => {
  const data1 = [
    {
      title: "Accordion 1",
      content: <Paragraph />,
    },
    {
      title: "Accordion 2",
      content: <Paragraph />,
    },
    {
      title: "Accordion 3",
      content: <Paragraph />,
    },
    {
      title: "Accordion 4",
      content: <Paragraph />,
    },
  ];
  const data2 = [
    {
      title: "Accordion 1",
      content: <Paragraph />,
    },
    {
      title: "Accordion 2",
      content: <Paragraph />,
    },
    {
      title: "Accordion 3",
      content: <Paragraph />,
    },
    {
      title: "Accordion 4",
      content: <Paragraph />,
    },
  ];
  return (
    <Fragment>
      <div className="blockcode">
        <div className="header">Default Accordion</div>
        <div className="description">Default Accordion</div>
        <div className="example">
          <CDBAccordion data={data1} />
        </div>
      </div>
      <div className="blockcode">
        <div className="header">Accordion without Icon</div>
        <div className="description">Accordion Without icon</div>
        <div className="example">
          <CDBAccordion data={data1} hideIcon />
        </div>
      </div>
    </Fragment>
  );
};
