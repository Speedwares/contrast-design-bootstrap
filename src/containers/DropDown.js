import React, { Fragment } from "react";
import CDBDropDown from "../components/DropDown";
import CDBDropDownToggle from "../components/DropDown/DropDownToggle";
import CDBDropDownMenu from "../components/DropDown/DropDownMenu";
import CDBDropDownItem from "../components/DropDown/DropDownItem";
import CDBLink from "../components/Link/Link";
export const DropDown = () => {
  return (
    <Fragment>
      <div
        style={{
          margin: "auto",
          width: "80%",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          minHeight: "100vh",
        }}
      >
        <div style={{ margin: "30px 0" }}>
          <CDBDropDown>
            <CDBDropDownToggle color="primary" caret>
              Primary Toggle Bar with caret
            </CDBDropDownToggle>
            <CDBDropDownMenu dropleft>
              This is the toogle menu to the left
            </CDBDropDownMenu>
          </CDBDropDown>
        </div>
        <div style={{ margin: "30px 0" }}>
          <CDBDropDown>
            <CDBDropDownToggle color="secondary" nav>
              Secondary Toggle Bar as nav
            </CDBDropDownToggle>
            <CDBDropDownMenu dropright>
              This is the toogle menu to the right
            </CDBDropDownMenu>
          </CDBDropDown>
        </div>
        <div style={{ margin: "30px 0" }}>
          <CDBDropDown style={{ margin: "30px 0" }}>
            <CDBDropDownToggle color="danger">
              Danger Toggle Bar
            </CDBDropDownToggle>
            <CDBDropDownMenu dropup>
              This is the toogle menu to the top
            </CDBDropDownMenu>
          </CDBDropDown>
        </div>
        <div style={{ margin: "30px 0" }}>
          <CDBDropDown style={{ margin: "30px 0" }}>
            <CDBDropDownToggle color="success">
              Success Toggle Bar
            </CDBDropDownToggle>
            <CDBDropDownMenu>
              This is the toogle menu to the bottom
            </CDBDropDownMenu>
          </CDBDropDown>
        </div>
        <div style={{ margin: "30px 0" }}>
          <CDBDropDown>
            <CDBDropDownToggle color="secondary" size="lg">
              Large Secondary Toggle Bar
            </CDBDropDownToggle>
            <CDBDropDownMenu dropright>
              This is the toogle menu to the right
            </CDBDropDownMenu>
          </CDBDropDown>
        </div>
        <div style={{ margin: "30px 0" }}>
          <CDBDropDown>
            <CDBDropDownToggle color="secondary" size="sm">
              small Secondary Toggle Bar
            </CDBDropDownToggle>
            <CDBDropDownMenu dropright>
              <CDBDropDownItem header>cold place</CDBDropDownItem>
              <CDBDropDownItem divider />
              <CDBDropDownItem disabled>
                First Item in cold place
              </CDBDropDownItem>
              <CDBDropDownItem disabled>second</CDBDropDownItem>
              <CDBDropDownItem toggle>second</CDBDropDownItem>
              <CDBDropDownItem>second</CDBDropDownItem>
              <CDBDropDownItem>
                <CDBLink to="/alert"> Alert</CDBLink>
              </CDBDropDownItem>
            </CDBDropDownMenu>
          </CDBDropDown>
        </div>
        <div style={{ margin: "30px 0" }}>
          <CDBDropDown>
            <CDBDropDownToggle color="danger" disabled>
              Disabled Danger Toggle Bar
            </CDBDropDownToggle>
            <CDBDropDownMenu dropright>
              This is the toogle menu to the right
            </CDBDropDownMenu>
          </CDBDropDown>
        </div>
      </div>
    </Fragment>
  );
};
