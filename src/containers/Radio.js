import React, { Fragment } from "react";
import CDBRadio from "../components/Radio";
import CDBRadioGroup from "../components/RadioGroup";

export const Radio = () => {
  return (
    <Fragment>
      <div className="blockcode">
        <div className="header">Single Radio Example</div>
        <div className="description"></div>
        <div className="example2">
          <CDBRadio
            value="Choice 1"
            fill="#eb7434"
            colorFill="#eb7434"
          ></CDBRadio>
          <CDBRadio value="Choice 2" fill="green" colorFill="red"></CDBRadio>
          <CDBRadio
            value="Choice 3"
            fill="#000000"
            colorFill="#000000"
          ></CDBRadio>
        </div>
      </div>
      <div className="blockcode">
        <div className="header">Radio Group</div>
        <div className="description"></div>
        <div className="example2">
          <CDBRadioGroup
            fill="#eb7434"
            colorFill="#eb7434"
            answers={["Choice 1", "Choice 2", "Choice 3", "Choice 4"]}
          ></CDBRadioGroup>
        </div>
      </div>
    </Fragment>
  );
};
