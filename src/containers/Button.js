import React, { Fragment } from "react";
import CDBBtn from "../components/Button";
import CDBIcon from "../components/Icon";

export const Button = () => {
  return (
    <Fragment>
      <div className="blockcode">
        <div className="header">Basic Styles</div>
        <div className="description">Basic contrast sytles for buttons</div>
        <div className="example">
          <CDBBtn color="primary">Primary</CDBBtn>
          <CDBBtn color="secondary">Secondary</CDBBtn>
          <CDBBtn color="success">Success</CDBBtn>
          <CDBBtn color="danger">Danger</CDBBtn>
          <CDBBtn color="dark">Dark</CDBBtn>
          <CDBBtn color="warning">Warning</CDBBtn>
          <CDBBtn color="info">Info</CDBBtn>
        </div>
      </div>
      <div className="blockcode">
        <div className="header">Circular Buttons</div>
        <div className="description">Circular Buttons</div>
        <div className="example">
          <CDBBtn color="primary" circle>
            Primary
          </CDBBtn>
          <CDBBtn color="secondary" circle>
            Secondary
          </CDBBtn>
          <CDBBtn color="success" circle>
            Success
          </CDBBtn>
          <CDBBtn color="danger" circle>
            Danger
          </CDBBtn>
          <CDBBtn color="dark" circle>
            Dark
          </CDBBtn>
          <CDBBtn color="warning" circle>
            Warning
          </CDBBtn>
          <CDBBtn color="info" circle>
            Info
          </CDBBtn>
        </div>
      </div>
      <div className="blockcode">
        <div className="header">Outline Buttons</div>
        <div className="description">Outline Buttons</div>
        <div className="example">
          <CDBBtn color="primary" outline>
            Primary
          </CDBBtn>
          <CDBBtn color="secondary" outline>
            Secondary
          </CDBBtn>
          <CDBBtn color="success" outline>
            Success
          </CDBBtn>
          <CDBBtn color="danger" outline>
            Danger
          </CDBBtn>
          <CDBBtn color="dark" outline>
            Dark
          </CDBBtn>
          <CDBBtn color="warning" outline>
            Warning
          </CDBBtn>
          <CDBBtn color="info" outline social>
            Info
          </CDBBtn>
        </div>
      </div>
      <div className="blockcode">
        <div className="header">Rounded Outline Buttons</div>
        <div className="description">Rounded Outline Buttons</div>
        <div className="example">
          <CDBBtn color="primary" circle outline>
            Primary
          </CDBBtn>
          <CDBBtn color="secondary" circle outline>
            Secondary
          </CDBBtn>
          <CDBBtn color="success" circle outline>
            Success
          </CDBBtn>
          <CDBBtn color="danger" circle outline>
            Danger
          </CDBBtn>
          <CDBBtn color="dark" circle outline>
            Dark
          </CDBBtn>
          <CDBBtn color="warning" circle outline>
            Warning
          </CDBBtn>
          <CDBBtn color="info" outline circle>
            Info
          </CDBBtn>
        </div>
      </div>
      <div className="blockcode">
        <div className="header">Flat Buttons</div>
        <div className="description">Flat Buttons</div>
        <div className="example">
          <CDBBtn color="primary" flat>
            Primary
          </CDBBtn>
          <CDBBtn color="secondary" flat>
            Secondary
          </CDBBtn>
          <CDBBtn color="success" flat>
            Success
          </CDBBtn>
          <CDBBtn color="danger" flat>
            Danger
          </CDBBtn>
          <CDBBtn color="dark" flat>
            Dark
          </CDBBtn>
          <CDBBtn color="warning" flat>
            Warning
          </CDBBtn>
          <CDBBtn color="info" flat>
            Info
          </CDBBtn>
        </div>
      </div>

      <div className="blockcode">
        <div className="header">Block Buttons</div>
        <div className="description">Block Buttons</div>
        <div className="example">
          <CDBBtn color="primary" block>
            Primary
          </CDBBtn>
        </div>
        <div className="example">
          <CDBBtn color="secondary" block>
            Secondary
          </CDBBtn>
        </div>
        <div className="example">
          <CDBBtn color="success" block>
            Success
          </CDBBtn>
        </div>
        <div className="example">
          <CDBBtn color="danger" block>
            Danger
          </CDBBtn>
        </div>
        <div className="example">
          <CDBBtn color="dark" block>
            Dark
          </CDBBtn>
        </div>
      </div>
      <div className="blockcode">
        <div className="header">Buttons with Icons</div>
        <div className="description"> Buttons with Icons</div>
        <div className="example">
          <CDBBtn color="warning">
            <CDBIcon icon="magic" className="ml-1" />
            Warning
          </CDBBtn>
          <CDBBtn color="info">
            Info
            <CDBIcon icon="magic" className="mr-1" />
          </CDBBtn>
        </div>
      </div>
      <div className="blockcode">
        <div className="header">Buttons Sizes</div>
        <div className="description"> Buttons Sizes</div>
        <div className="example">
          <CDBBtn color="primary" size="large">
            Large button
          </CDBBtn>
          <CDBBtn color="secondary">Normal button</CDBBtn>
          <CDBBtn color="success" size="small">
            Small button
          </CDBBtn>
        </div>
      </div>
      <div className="blockcode">
        <div className="header">Buttons with Icons</div>
        <div className="description"> Buttons with Icons</div>
        <div className="example">
          <CDBBtn color="warning">
            <CDBIcon icon="magic" className="ml-1" />
            Warning
          </CDBBtn>
          <CDBBtn color="info">
            Info
            <CDBIcon icon="magic" className="mr-1" />
          </CDBBtn>
        </div>
      </div>
      <div className="blockcode">
        <div className="header">Disabled Button</div>
        <div className="description">Disabled Buttons</div>
        <div className="example">
          <CDBBtn color="secondary" disabled>
            Disabled Button
          </CDBBtn>
        </div>
      </div>
    </Fragment>
  );
};
