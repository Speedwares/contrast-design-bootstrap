import React, { Fragment } from "react";
import CDBNotification from "../components/Notification";

export const Notification = () => {
  return (
    <Fragment>
      <div
        style={{
          margin: "auto",
          width: "80%",
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
        }}
      >
        <div style={{ margin: " 10px 0" }}>
          <CDBNotification
            show
            fade
            iconClassName="text-primary"
            title="Bootstrap"
            message="Hello, world! This is a primary message."
            text="11 mins ago"
          />
        </div>
        <div style={{ margin: " 10px 0" }}>
          <CDBNotification
            show
            fade
            iconClassName="text-danger"
            title="Bootstrap"
            message="Hello, world! This is a danger message."
            text="11 mins ago"
          />
        </div>
        <div style={{ margin: " 10px 0" }}>
          <CDBNotification
            show
            fade
            iconClassName="text-secondary"
            title="Bootstrap"
            message="Hello, world! This is a secondary message."
            text="11 mins ago"
          />
        </div>
        <div style={{ margin: " 10px 0" }}>
          <CDBNotification
            icon="envelope"
            show
            fade
            iconClassName="text-primary"
            title="Bootstrap"
            message="Heads up, toasts will stack automatically"
            text="2 seconds ago"
          />
        </div>
      </div>
    </Fragment>
  );
};
