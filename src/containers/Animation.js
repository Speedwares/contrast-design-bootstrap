import React, { Fragment } from "react";
import CDBAnimation from "../components/Animation";

export const Animation = () => {
  return (
    <Fragment>
      <div className="blockcode">
        <div className="header">Animation Types</div>
        <div className="description"></div>
        <div className="example2">
          <CDBAnimation type="bounce" infinite>
            <img src="img/ball.png" width="50" height="50" />
          </CDBAnimation>
          <CDBAnimation type="flash" infinite>
            <img src="img/ball.png" width="50" height="50" />
          </CDBAnimation>
          <CDBAnimation type="pulse" infinite>
            <img src="img/ball.png" width="50" height="50" />
          </CDBAnimation>
          <CDBAnimation type="rubberBand" infinite>
            <img src="img/ball.png" width="50" height="50" />
          </CDBAnimation>
          <CDBAnimation type="shake" infinite>
            <img src="img/ball.png" width="50" height="50" />
          </CDBAnimation>
          <CDBAnimation type="headShake" infinite>
            <img src="img/ball.png" width="50" height="50" />
          </CDBAnimation>
          <CDBAnimation type="swing" infinite>
            <img src="img/ball.png" width="50" height="50" />
          </CDBAnimation>

          <CDBAnimation type="tada" infinite>
            <img src="img/ball.png" width="50" height="50" />
          </CDBAnimation>
          <CDBAnimation type="wobble" infinite>
            <img src="img/ball.png" width="50" height="50" />
          </CDBAnimation>
          <CDBAnimation type="jello" infinite>
            <img src="img/ball.png" width="50" height="50" />
          </CDBAnimation>
          <CDBAnimation type="heartBeat" infinite>
            <img src="img/ball.png" width="50" height="50" />
          </CDBAnimation>
        </div>
      </div>
    </Fragment>
  );
};
