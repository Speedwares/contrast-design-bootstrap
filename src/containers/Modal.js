import React, { Component } from "react";
import CDBModal from "../components/Modal";
import CDBModalHeader from "../components/Modal/ModalHeader";
import CDBModalFooter from "../components/Modal/ModalFooter";
import CDBModalBody from "../components/Modal/ModalBody";
import CDBBtn from "../components/Button";

export class Modal extends Component {
  state = {
    modal: false,
  };

  toggle = () => {
    this.setState({
      modal: !this.state.modal,
    });
  };

  render() {
    return (
      <div
        style={{
          margin: "auto",
          width: "80%",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          minHeight: "100vh",
        }}
      >
        <CDBBtn onClick={this.toggle}>Modal</CDBBtn>
        <CDBModal
          isOpen={this.state.modal}
          toggle={this.toggle}
          centered
          size="sm"
          fade
        >
          <CDBModalHeader toggle={this.toggle}>CDBModal title</CDBModalHeader>
          <CDBModalBody>Write some awesome text here</CDBModalBody>
          <CDBModalFooter>
            <CDBBtn color="secondary" onClick={this.toggle}>
              Close
            </CDBBtn>
          </CDBModalFooter>
        </CDBModal>
      </div>
    );
  }
}
