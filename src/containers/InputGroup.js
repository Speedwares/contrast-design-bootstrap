import React from "react";
import CDBInputGroup from "../components/InputGroup";
import CDBInput from "../components/Input";

export const InputGroup = () => {
  return (
    <div style={{ width: "80%", margin: "0 auto" }}>
      <CDBInputGroup
        material
        hint="Recipient's username"
        containerClassName="mb-3 mt-0"
        append="@example.com"
      />
      <CDBInputGroup
        material
        containerClassName="flex-nowrap mb-3"
        prepend="@"
        hint="Username"
      />
      <CDBInputGroup
        label="Run function on click icon"
        icon="bell"
        onIconClick={() => alert("Wait! This is an alert!")}
      />
      <CDBInputGroup
        material
        containerClassName="mb-2 mt-0"
        prepend="Small"
        size="sm"
      />
      <CDBInputGroup
        material
        containerClassName="mb-2 mt-0"
        prepend="Large"
        size="lg"
      />
      <CDBInputGroup
        material
        prepend={
          <div className="input-group-text md-addon">
            <CDBInput label type="checkbox" id="checkboxMaterial1" />
          </div>
        }
      />
      <CDBInputGroup />
    </div>
  );
};
