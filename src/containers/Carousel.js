import React, { Fragment } from "react";
import CDBContainer from "../components/Container";
import CDBCarousel from "../components/Carousel";
import CDBCarouselItem from "../components/Carousel/CarouselItem";
import CDBCarouselInner from "../components/Carousel/CarouselInner";
import CDBView from "../components/View";

export const Carousel = () => {
  return (
    <Fragment>
      <CDBContainer>
        <CDBCarousel
          activeItem={1}
          length={3}
          showControls={true}
          showIndicators={true}
          className="z-depth-1"
          slide
        >
          <CDBCarouselInner>
            <CDBCarouselItem itemId="1">
              <CDBView>
                <img
                  className="d-block w-100"
                  src="https://mdbootstrap.com/img/Photos/Slides/img%20(45).jpg"
                  alt="First slide"
                />
              </CDBView>
            </CDBCarouselItem>
            <CDBCarouselItem itemId="2">
              <CDBView>
                <img
                  className="d-block w-100"
                  src="https://mdbootstrap.com/img/Photos/Slides/img%20(46).jpg"
                  alt="Second slide"
                />
              </CDBView>
            </CDBCarouselItem>
            <CDBCarouselItem itemId="3">
              <CDBView>
                <img
                  className="d-block w-100"
                  src="https://mdbootstrap.com/img/Photos/Slides/img%20(47).jpg"
                  alt="Third slide"
                />
              </CDBView>
            </CDBCarouselItem>
          </CDBCarouselInner>
        </CDBCarousel>
      </CDBContainer>
    </Fragment>
  );
};
