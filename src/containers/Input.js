import React, { Fragment } from "react";
import CDBInput from "../components/Input";

export const Input = () => {
  return (
    <Fragment>
      <div className="blockcode">
        <div className="header">Default Input types</div>
        <div className="description"></div>
        <div className="example2">
          <CDBInput label="Username" />
          <CDBInput hint="Your e-mail" type="email" />
          <CDBInput type="number" />
          <CDBInput outline type="text" />
          <CDBInput background type="text" />
          <CDBInput type="checkbox" />
          <CDBInput type="textarea" />
        </div>
      </div>
      <div className="blockcode">
        <div className="header">Input Sizing</div>
        <div className="description"></div>
        <div className="example2">
          <CDBInput label="Your e-mail" type="email" size="sm" />
          <CDBInput type="number" label="Your Number" size="lg" />
        </div>
      </div>
      <div className="blockcode">
        <div className="header">Input with Icons</div>
        <div className="description"></div>
        <div className="example2">
          <CDBInput label="Username" icon="user" />
          <CDBInput label="Password" type="password" icon="bell" />
        </div>
      </div>
      <div className="blockcode">
        <div className="header">Input with Outline</div>
        <div className="description"></div>
        <div className="example2">
          <CDBInput outline label="Your e-mail" type="email" />
          <CDBInput outline type="number" label="Your Number" />
        </div>
      </div>
      <div className="blockcode">
        <div className="header">Placeholder</div>
        <div className="description"></div>
        <div className="example2">
          <CDBInput hint="Your e-mail" type="email" />
          <CDBInput type="number" hint="Your Number" />
        </div>
      </div>

      <div className="blockcode">
        <div className="header">Disabled Input</div>
        <div className="description"></div>
        <div className="example2">
          <CDBInput hint="Your e-mail" type="email" disabled />
          <CDBInput type="number" hint="Your Number" disabled />
        </div>
      </div>
      <div className="blockcode">
        <div className="header">TextArea</div>
        <div className="description"></div>
        <div className="example2">
          <CDBInput label="Your name" type="textarea" />
          <CDBInput
            label="Your story"
            type="textarea"
            outline
            rows={20}
            cols={30}
          />
        </div>
      </div>
      <div className="blockcode">
        <div className="header">Background</div>
        <div className="description"></div>
        <div className="example2">
          <CDBInput hint="Your e-mail" type="email" background />
          <CDBInput type="number" hint="Your Number" background />
        </div>
      </div>
    </Fragment>
  );
};
