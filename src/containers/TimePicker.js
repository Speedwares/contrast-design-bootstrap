import React, { Component } from "react";
import CDBTimePicker from "../components/TimePicker2";
class TimePickerPage extends Component {
  static propTypes = {};

  render() {
    return (
      <div
        style={{
          margin: "auto",
          width: "80%",
          display: "flex",
          flexDirection: "column",
        }}
      >
        <CDBTimePicker theme="classic" colorPalette="dark" />
      </div>
    );
  }
}
export default TimePickerPage;
