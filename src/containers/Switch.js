import React, { Fragment } from "react";
import CDBSwitch from "../components/Switch";

export const Switch = () => {
  return (
    <Fragment>
      <div
        style={{
          margin: "auto",
          width: "80%",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          minHeight: "100vh",
        }}
      >
        <CDBSwitch />
        <CDBSwitch checked />
      </div>
    </Fragment>
  );
};
