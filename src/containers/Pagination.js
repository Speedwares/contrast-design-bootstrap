import React, { Fragment } from "react";
import CDBPagination from "../components/Pagination";
import CDBPageItem from "../components/Pagination/PageItem";
import CDBPageLink from "../components/Pagination/PageLink";

export const Pagination = () => {
  return (
    <Fragment>
      <div
        style={{
          margin: "auto",
          width: "80%",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          minHeight: "100vh",
        }}
      >
        {" "}
        <CDBPagination circle>
          <CDBPageLink>Prev</CDBPageLink>
          <CDBPageItem>1</CDBPageItem>
          <CDBPageItem>2</CDBPageItem>
          <CDBPageItem>3</CDBPageItem>
          <CDBPageLink>Next</CDBPageLink>
        </CDBPagination>
        <CDBPagination circle size="big" color="success">
          <CDBPageLink>Prev</CDBPageLink>
          <CDBPageItem>1</CDBPageItem>
          <CDBPageItem>2</CDBPageItem>
          <CDBPageItem>3</CDBPageItem>
          <CDBPageLink>Next</CDBPageLink>
        </CDBPagination>
        <CDBPagination size="small" color="secondary">
          <CDBPageLink>Prev</CDBPageLink>
          <CDBPageItem>1</CDBPageItem>
          <CDBPageItem>2</CDBPageItem>
          <CDBPageItem>3</CDBPageItem>
          <CDBPageLink>Next</CDBPageLink>
        </CDBPagination>
        <CDBPagination color="warning">
          <CDBPageLink>Prev</CDBPageLink>
          <CDBPageItem>1</CDBPageItem>
          <CDBPageItem>2</CDBPageItem>
          <CDBPageItem>3</CDBPageItem>
          <CDBPageLink>Next</CDBPageLink>
        </CDBPagination>
        <CDBPagination color="danger">
          <CDBPageLink>Prev</CDBPageLink>
          <CDBPageItem>1</CDBPageItem>
          <CDBPageItem>2</CDBPageItem>
          <CDBPageItem>3</CDBPageItem>
          <CDBPageLink>Next</CDBPageLink>
        </CDBPagination>
        <CDBPagination>
          <CDBPageLink>Prev</CDBPageLink>
          <CDBPageItem>1</CDBPageItem>
          <CDBPageItem>2</CDBPageItem>
          <CDBPageItem>3</CDBPageItem>
          <CDBPageLink>Next</CDBPageLink>
        </CDBPagination>
      </div>
    </Fragment>
  );
};
