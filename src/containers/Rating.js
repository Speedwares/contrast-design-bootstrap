import React, { Fragment } from "react";
import CDBRating from "../components/Rating";

export const Rating = () => {
  return (
    <Fragment>
      <div
        style={{
          margin: "auto",
          width: "80%",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          minHeight: "100vh",
        }}
      >
       
          
            <CDBRating iconRegular />
            <CDBRating  iconFaces
        fillClassName='black-text'
        iconRegular />
        <CDBRating feedback/>
      
        </div>
    </Fragment>
  );
};
