import React, { Fragment } from "react";
import CDBBtn from "../components/Button";
import CDBBtnGrp from "../components/ButtonGroup";
import CDBBtnTb from "../components/ButtonToolbar";

export const ButtonToolbar = () => {
  return (
    <Fragment>
      <CDBBtnTb>
        <CDBBtnGrp className="mr-2">
          <CDBBtn color="primary">click me</CDBBtn>
          <CDBBtn color="secondary">click me</CDBBtn>
          <CDBBtn color="success">click me</CDBBtn>
        </CDBBtnGrp>
        <CDBBtnGrp size="sm" className="mr-2">
          <CDBBtn color="primary">click me</CDBBtn>
          <CDBBtn color="secondary">click me</CDBBtn>
          <CDBBtn color="success">click me</CDBBtn>
        </CDBBtnGrp>
        <CDBBtnGrp >
          <CDBBtn color="danger">click me</CDBBtn>
          <CDBBtn color="dark">click me</CDBBtn>
          <CDBBtn color="warning">click me</CDBBtn>
        </CDBBtnGrp>
      </CDBBtnTb>
    </Fragment>
  );
};
