import React, { useState } from "react";
import CDBDatePicker from "../components/DatePicker";

export const DatePickerPage = () => {
  const [value, onChange] = useState(new Date());
  console.log(value);

  return (
    <>
      <div
        style={{
          margin: "auto",
          width: "80%",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          minHeight: "100vh",
        }}
      >
        <CDBDatePicker onChange={onChange} value={value} />
      </div>
    </>
  );
};
