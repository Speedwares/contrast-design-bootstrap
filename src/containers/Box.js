import React, { Fragment } from "react";
import { CDBBox } from "../components/Box/Box";

export const Box = () => {
  return (
    <Fragment>
      <div
        style={{
          margin: "auto",
          width: "80%",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          minHeight: "100vh",
        }}
      >
        <CDBBox style={{ height: "100px" }} color="#c4c4c4" bg="green" mt={4}>
          Hello and my friends
        </CDBBox>
      </div>
    </Fragment>
  );
};
