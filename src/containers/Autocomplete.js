import React, { Fragment } from "react";
import CDBAutocomplete from "../components/Autocomplete";
import CDBContainer from "../components/Container";
import CDBInput from "../components/Input";
import CDBCard from "../components/Card";
import CDBBtn from "../components/Button";
import CDBCardBody from "../components/Card/CardBody";
import CDBIcon from "../components/Icon";

export const Autocomplete = () => {
  const logValue = (value) => {
    console.log(value);
  };
  const food = [
    "Rice",
    "Porridge",
    "Hamburger",
    "Doughnuts",
    "Tacos",
    "Soup",
    "Oreos",
    "Cereal",
  ];
  return (
    <Fragment>
      <div className="blockcode">
        <div className="header">Default Autocomplete</div>
        <div className="description"></div>
        <div className="example">
          {" "}
          <CDBContainer style={{ position: "relative" }}>
            <CDBAutocomplete
              suggestions={["White", "Black", "Green", "Blue", "Yellow", "Red"]}
              getValue={logValue}
              label="Choose a Color"
            />
          </CDBContainer>
        </div>
      </div>
      <div className="blockcode">
        <div className="header">Usage</div>
        <div className="description"></div>
        <div className="example">
          <CDBCard style={{ width: "100%" }}>
            <CDBCardBody className="mx-4">
              <div className="text-center">
                <h3 className="dark-grey-text mb-5">
                  <strong>Sign in</strong>
                </h3>
              </div>
              <CDBInput
                label="Your email"
                group
                type="email"
                validate
                error="wrong"
                success="right"
              />
              <CDBInput
                label="Your password"
                group
                type="password"
                validate
                containerClass="mb-0"
              />
              <CDBAutocomplete label="Your favourite food" suggestions={food} />
              <div className="text-center pt-3 mb-3">
                <CDBBtn
                  type="button"
                  gradient="blue"
                  className="btn-block z-depth-1a"
                >
                  Sign in
                </CDBBtn>
              </div>
              <p
                className="dark-grey-text text-right d-flex justify-content-center mb-3 pt-2"
                style={{ fontSize: "0.8rem" }}
              >
                or Sign up with:
              </p>
              <div className="row my-3 d-flex justify-content-center">
                <CDBBtn
                  type="button"
                  color="white"
                  circle
                  className="mr-md-3 z-depth-1a"
                >
                  <CDBIcon
                    fab
                    icon="facebook-f"
                    className="blue-text text-center"
                  />
                </CDBBtn>
                <CDBBtn
                  type="button"
                  color="white"
                  circle
                  className="mr-md-3 z-depth-1a"
                >
                  <CDBIcon fab icon="twitter" className="blue-text" />
                </CDBBtn>
                <CDBBtn
                  type="button"
                  color="white"
                  circle
                  className="z-depth-1a"
                >
                  <CDBIcon fab icon="google-plus-g" className="blue-text" />
                </CDBBtn>
              </div>
            </CDBCardBody>
          </CDBCard>
        </div>
      </div>
    </Fragment>
  );
};
