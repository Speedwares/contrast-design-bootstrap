import React, { Fragment } from "react";
import CDBBtn from "../components/Button";
import CDBBtnGrp from "../components/ButtonGroup";

export const ButtonGroup = () => {
  return (
    <Fragment>
      <CDBBtnGrp>
        <CDBBtn color="primary">click me</CDBBtn>
        <CDBBtn color="secondary">click me</CDBBtn>
        <CDBBtn color="success">click me</CDBBtn>
        <CDBBtn color="danger">click me</CDBBtn>
        <CDBBtn color="dark">click me</CDBBtn>
        <CDBBtn color="warning">click me</CDBBtn>
      </CDBBtnGrp>
      <CDBBtnGrp vertical="true" size="lg">
        <CDBBtn color="primary">click me</CDBBtn>
        <CDBBtn color="secondary">click me</CDBBtn>
        <CDBBtn color="success">click me</CDBBtn>
        <CDBBtn color="danger">click me</CDBBtn>
        <CDBBtn color="dark">click me</CDBBtn>
        <CDBBtn color="warning">click me</CDBBtn>
      </CDBBtnGrp>
    </Fragment>
  );
};
