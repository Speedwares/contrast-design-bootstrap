import React, { useState } from "react";
import CDBSlider from "../components/Slider";

export const Slider = () => {
  const [value, setValue] = useState(0);
  return (
    <div
      style={{
        margin: "auto",
        width: "80%",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        minHeight: "100vh",
      }}
    >
      <CDBSlider value={value} setValue={setValue} style={{ width: "100%" }} />
    </div>
  );
};
