import React, {useState} from "react";
import CDBSidebar from "../components/Sidebar";
import CDBSidebarContent from "../components/Sidebar/SidebarContent";
import CDBSidebarFooter from "../components/Sidebar/SidebarFooter";
import CDBSidebarHeader from "../components/Sidebar/SidebarHeader";
import CDBSidebarSubMenu from "../components/Sidebar/SidebarSubMenu";
import CDBSidebarMenu from "../components/Sidebar/SidebarMenu";
import CDBSidebarMenuItem from "../components/Sidebar/SidebarMenuItem";
import CDBContainer from "../components/Container";
import CDBIcon from "../components/Icon";
import sidebarBg from '../bg1.jpg';




export const SidebarContainer = () => {

    const [rtl, setRtl] = useState(false);
    const [collapsed, setCollapsed] = useState(false);
    const [image, setImage] = useState(true);
    const [toggled, setToggled] = useState(false);

    const handleCollapsedChange = (checked) => {
        setCollapsed(checked);
    };

    const handleRtlChange = (checked) => {
        setRtl(checked);
    };

    const handleImageChange = (checked) => {
        setImage(checked);
    };

    const handleToggleSidebar = (value) => {
        setToggled(value);
    };

    return (
      <CDBContainer>
        <div className={`app ${rtl ? "rtl" : ""} ${toggled ? "toggled" : ""}`}>
          <CDBSidebar
            image={image ? sidebarBg : false}
            rtl={rtl}
            collapsed={collapsed}
            toggled={toggled}
            breakPoint="md"
            onToggle={handleToggleSidebar}
            style={{background: "#fff"}}
          >
            <CDBSidebarHeader>
              <div
                style={{
                  padding: "24px",
                  textTransform: "uppercase",
                  fontWeight: "bold",
                  fontSize: 14,
                  letterSpacing: "1px",
                  overflow: "hidden",
                  textOverflow: "ellipsis",
                  whiteSpace: "nowrap"
                }}
              >
                The title
              </div>
            </CDBSidebarHeader>

            <CDBSidebarContent>
              <CDBSidebarMenu iconShape="circle">
                <CDBSidebarMenuItem
                  suffix={<span className="badge red"> new </span>}
                >
                  Dashboard
                </CDBSidebarMenuItem>
                <CDBSidebarMenuItem >
                  Components
                </CDBSidebarMenuItem>
              </CDBSidebarMenu>
              <CDBSidebarMenu iconShape="circle">
                <CDBSidebarSubMenu
                  title="Sidemenu"
                >
                  <CDBSidebarMenuItem> submenu 1</CDBSidebarMenuItem>
                  <CDBSidebarMenuItem> submenu 2</CDBSidebarMenuItem>
                  <CDBSidebarMenuItem> submenu 3</CDBSidebarMenuItem>
                </CDBSidebarSubMenu>
                <CDBSidebarSubMenu
                  prefix={<span className="badge">pre</span>}
                  title="Sidemenu2"
                >
                  <CDBSidebarMenuItem>submenu 1</CDBSidebarMenuItem>
                  <CDBSidebarMenuItem>submenu 2</CDBSidebarMenuItem>
                  <CDBSidebarMenuItem>submenu 3</CDBSidebarMenuItem>
                </CDBSidebarSubMenu>
                <CDBSidebarSubMenu
                  title="MultiLevel with Icon"
                  icon={<CDBIcon icon="list" />}
                >
                  <CDBSidebarMenuItem>submenu 1 </CDBSidebarMenuItem>
                  <CDBSidebarMenuItem>submenu 2 </CDBSidebarMenuItem>
                  <CDBSidebarSubMenu title="submenu 3">
                    <CDBSidebarMenuItem>submenu 3.1 </CDBSidebarMenuItem>
                    <CDBSidebarMenuItem>submenu 3.2 </CDBSidebarMenuItem>
                    <CDBSidebarSubMenu title="3.3">
                      <CDBSidebarMenuItem>submenu 3.3.1 </CDBSidebarMenuItem>
                      <CDBSidebarMenuItem>submenu 3.3.2 </CDBSidebarMenuItem>
                      <CDBSidebarMenuItem>submenu 3.3.3 </CDBSidebarMenuItem>
                    </CDBSidebarSubMenu>
                  </CDBSidebarSubMenu>
                </CDBSidebarSubMenu>
              </CDBSidebarMenu>
            </CDBSidebarContent>

            <CDBSidebarFooter style={{ textAlign: "center" }}>
              <div
                className="sidebar-btn-wrapper"
                style={{
                  padding: "20px 24px"
                }}>    
                            Sidebar Footer
              </div>
            </CDBSidebarFooter>
          </CDBSidebar>
        </div>
      </CDBContainer>
    );
};