import React, { Fragment } from "react";
import CDBProgress from "../components/Progress";

export const Progress = () => {
  return (
    <Fragment>
      <div
        style={{
          margin: "auto",
          width: "80%",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          minHeight: "100vh",
        }}
      >
        <div style={{ margin: "20px 0", width: "100%" }}>
          <CDBProgress
            value={25}
            wrapperStyle={{ width: "100%", background: "#fff" }}
            height={20}
          ></CDBProgress>
        </div>

        <div style={{ margin: "20px 0", width: "100%" }}>
          <CDBProgress
            value={50}
            wrapperStyle={{ width: "100%", background: "#fff" }}
            height={20}
          ></CDBProgress>
        </div>
        <div style={{ margin: "20px 0", width: "100%" }}>
          <CDBProgress
            value={75}
            wrapperStyle={{ width: "100%", background: "#fff" }}
            height={30}
          ></CDBProgress>
        </div>
        <div style={{ margin: "20px 0", width: "100%" }}>
          <CDBProgress
            value={85}
            colors="secondary"
            wrapperStyle={{ width: "100%", background: "#fff" }}
            height={20}
          ></CDBProgress>
        </div>
        <div style={{ margin: "20px 0", width: "100%" }}>
          <CDBProgress
            value={45}
            colors="success"
            wrapperStyle={{ width: "100%", background: "#fff" }}
            height={10}
          ></CDBProgress>
        </div>
        <div style={{ margin: "20px 0", width: "100%" }}>
          <CDBProgress
            value={60}
            colors="danger"
            wrapperStyle={{ width: "100%", background: "#fff" }}
            height={20}
          ></CDBProgress>
        </div>
        <div style={{ margin: "20px 0", width: "100%" }}>
          <CDBProgress
            value={85}
            colors="warning"
            wrapperStyle={{ width: "100%", background: "#fff" }}
            height={5}
          ></CDBProgress>
        </div>
      </div>
    </Fragment>
  );
};
