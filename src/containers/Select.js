import React, { Fragment, useState } from "react";
import CDBSelect from "../components/Select";

export const Select = () => {
  const [option] = useState([
    {
      text: "Option 1",
      value: "1",
    },
    {
      text: "Option 2",
      value: "2",
    },
    {
      text: "Option 3",
      value: "3",
    },
  ]);
  return (
    <Fragment>
      <div
        style={{
          margin: "auto",
          width: "80%",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          minHeight: "100vh",
        }}
      >
        <CDBSelect options={option} selected="Choose an option"></CDBSelect>
      </div>
    </Fragment>
  );
};
