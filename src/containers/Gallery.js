import React, { Fragment } from "react";
import CDBGallery from "../components/Gallery";

export const Gallery = () => {
  return (
    <Fragment>
      <div
        style={{
          margin: "auto",
          width: "80%",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          minHeight: "100vh",
        }}
      >
        <CDBGallery></CDBGallery>
      </div>
    </Fragment>
  );
};
