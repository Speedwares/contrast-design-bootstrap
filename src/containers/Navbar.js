import React, { Fragment, useState } from "react";
import CDBContainer from "../components/Container";
import CDBNavbar from "../components/Navbar";
import CDBNavBrand from "../components/Navbar/NavBrand";
import CDBNavbarNav from "../components/Navbar/NavbarNav";
import CDBNavToggle from "../components/Navbar/NavToggle";
import CDBNavItem from "../components/Navbar/NavItem";
import CDBNavLink from "../components/Navbar/NavLink";
import CDBIcon from "../components/Icon";

import { BrowserRouter as Router } from 'react-router-dom';

export const NavbarExample = () => {
    const [collapse, setCollapse] = useState(false);

    const bgPink = { backgroundColor: '#e91e63' }
    const container = { height: 1300 }

    return (
            <Fragment>
            <div>
                <Router>
                    <header>
                        <CDBNavbar style={bgPink} dark expand="md" scrolling fixed="top">
                            <CDBNavBrand href="/">
                                <strong>Navbar</strong>
                            </CDBNavBrand>
                            <CDBNavToggle onClick={()=> { setCollapse(!collapse) }} />
                                <CDBNavbarNav left>
                                    <CDBNavItem active>
                                        <CDBNavLink to="#">Home</CDBNavLink>
                                    </CDBNavItem>
                                    <CDBNavItem>
                                        <CDBNavLink to="#">Features</CDBNavLink>
                                    </CDBNavItem>
                                    <CDBNavItem>
                                        <CDBNavLink to="#">Pricing</CDBNavLink>
                                    </CDBNavItem>
                                    <CDBNavItem>
                                        <CDBNavLink to="#">Options</CDBNavLink>
                                    </CDBNavItem>
                                </CDBNavbarNav>
                                <CDBNavbarNav right>
                                    <CDBNavItem>
                                        <CDBNavLink to="#"><CDBIcon fab icon="facebook-f" /></CDBNavLink>
                                    </CDBNavItem>
                                    <CDBNavItem>
                                        <CDBNavLink to="#"><CDBIcon fab icon="twitter" /></CDBNavLink>
                                    </CDBNavItem>
                                    <CDBNavItem>
                                        <CDBNavLink to="#"><CDBIcon fab icon="instagram" /></CDBNavLink>
                                    </CDBNavItem>
                                </CDBNavbarNav>
                        </CDBNavbar>
                    </header>
                </Router>
                <CDBContainer style={container} className="text-center mt-5 pt-5">
                    <h2>This Navbar is fixed</h2>
                 </CDBContainer>
            </div>
        </Fragment>
        );
}
