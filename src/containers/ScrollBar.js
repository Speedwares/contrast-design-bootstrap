import React, { Fragment, useState } from "react";
import CDBScrollBar from "../components/ScrollBar";

export const ScrollBar = () => {
  return (
    <Fragment>
      <div
        style={{
          margin: "auto",
          width: "80%",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          minHeight: "100vh",
        }}
      >
        <CDBScrollBar></CDBScrollBar>
      </div>
    </Fragment>
  );
};
