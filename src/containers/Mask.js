import React, { Component } from "react";
import CDBView from "../components/View";
import CDBMask from "../components/Mask";

export class Mask extends Component {
  render() {
    return (
      <div
        style={{
          margin: "auto",
          width: "80%",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          minHeight: "100vh",
        }}
      >
        <CDBView>
          <img
            src="https://mdbootstrap.com/img/Photos/Others/nature-sm.jpg"
            className="img-fluid"
            alt=""
          />
          <CDBMask pattern="pattern1" className="flex-center" />
        </CDBView>
      </div>
    );
  }
}
