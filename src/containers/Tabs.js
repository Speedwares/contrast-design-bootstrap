import React, { Component } from "react";
import CDBContainer from "../components/Container";
import CDBTabPane from "../components/TabPane";
import CDBTabContent from "../components/TabContent";
import CDBNav from "../components/Nav";
import CDBNavItem from "../components/Navbar/NavItem";
import CDBNavLink from "../components/Navbar/NavLink";

class Tabs extends Component {
  state = {
    activeItem: "1",
  };

  toggle = (tab) => (e) => {
    if (this.state.activeItem !== tab) {
      this.setState({
        activeItem: tab,
      });
    }
  };

  render() {
    return (
      <CDBContainer
        style={{
          background: "white",
          border: "none",
          marginTop: "20px",
          padding: "10px",
        }}
      >
        <CDBNav className="nav-tabs mt-5">
          <CDBNavItem>
            <CDBNavLink
              link
              to="#"
              active={this.state.activeItem === "1"}
              onClick={this.toggle("1")}
              role="tab"
            >
              Home
            </CDBNavLink>
          </CDBNavItem>
          <CDBNavItem>
            <CDBNavLink
              link
              to="#"
              active={this.state.activeItem === "2"}
              onClick={this.toggle("2")}
              role="tab"
            >
              Profile
            </CDBNavLink>
          </CDBNavItem>
          <CDBNavItem>
            <CDBNavLink
              link
              to="#"
              active={this.state.activeItem === "3"}
              onClick={this.toggle("3")}
              role="tab"
            >
              Profile
            </CDBNavLink>
          </CDBNavItem>
        </CDBNav>
        <CDBTabContent activeItem={this.state.activeItem}>
          <CDBTabPane tabId="1" role="tabpanel">
            <p className="mt-2">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil
              odit magnam minima, soluta doloribus reiciendis molestiae placeat
              unde eos molestias. Quisquam aperiam, pariatur. Tempora, placeat
              ratione porro voluptate odit minima.
            </p>
          </CDBTabPane>
          <CDBTabPane tabId="2" role="tabpanel">
            <p className="mt-2">
              Quisquam aperiam, pariatur. Tempora, placeat ratione porro
              voluptate odit minima. Lorem ipsum dolor sit amet, consectetur
              adipisicing elit. Nihil odit magnam minima, soluta doloribus
              reiciendis molestiae placeat unde eos molestias.
            </p>
            <p>
              Quisquam aperiam, pariatur. Tempora, placeat ratione porro
              voluptate odit minima. Lorem ipsum dolor sit amet, consectetur
              adipisicing elit. Nihil odit magnam minima, soluta doloribus
              reiciendis molestiae placeat unde eos molestias.
            </p>
          </CDBTabPane>
          <CDBTabPane tabId="3" role="tabpanel">
            <p className="mt-2">
              Quisquam aperiam, pariatur. Tempora, placeat ratione porro
              voluptate odit minima. Lorem ipsum dolor sit amet, consectetur
              adipisicing elit. Nihil odit magnam minima, soluta doloribus
              reiciendis molestiae placeat unde eos molestias.
            </p>
          </CDBTabPane>
        </CDBTabContent>
      </CDBContainer>
    );
  }
}
export default Tabs;
