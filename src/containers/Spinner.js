import React from "react";
import CDBSpinner from "../components/Spinner";

export const Spinner = () => {
  return (
    <React.Fragment>
      <div className="blockcode">
        <div className="header">Spinner Types</div>
        <div className="description"></div>
        <div className="example">
          <CDBSpinner />
          <CDBSpinner danger />
          <CDBSpinner success />
          <CDBSpinner warning />
          <CDBSpinner info />
          <CDBSpinner dark />
          <CDBSpinner secondary />
        </div>
      </div>
      <div className="blockcode">
        <div className="header">Multicolor Spinner</div>
        <div className="description"></div>
        <div className="example">
          <CDBSpinner multicolor />
        </div>
      </div>
      <div className="blockcode">
        <div className="header">Size Variations</div>
        <div className="description"></div>
        <div className="example">
          <CDBSpinner size="big" />
          <CDBSpinner secondary />
          <CDBSpinner success size="small" />
        </div>
      </div>
    </React.Fragment>
  );
};
