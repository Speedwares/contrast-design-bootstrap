import React from "react";
import CDBIframe from "../components/Iframe";

export const Iframe = () => {
  return (
    <div style={{ width: "80%", margin: "0 auto", background: "#282c34" }}>
      <CDBIframe src="https://www.youtube.com/embed/xnczyP2jSR0"></CDBIframe>
    </div>
  );
};
