import React, { Fragment } from "react";
import CDBAlert from "../components/Alert";

export const Alert = () => {
  return (
    <Fragment>
      <div
        style={{
          margin: "auto",
          width: "80%",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          minHeight: "100vh",
        }}
      >
        <CDBAlert color="primary">
          A simple alert built with contrast design check it out!
        </CDBAlert>
        <CDBAlert color="secondary">
          A simple alert built with contrast design check it out!
        </CDBAlert>
        <CDBAlert color="success">
          A simple alert built with contrast design check it out!
        </CDBAlert>
        <CDBAlert color="danger">
          A simple alert built with contrast design check it out!
        </CDBAlert>
        <CDBAlert color="warning">
          A simple alert built with contrast design check it out!
        </CDBAlert>
      </div>
    </Fragment>
  );
};
