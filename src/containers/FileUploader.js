import React from "react";
import CDBFileUploader from "../components/FileUploader";

export const FileUploader = () => {
  return (
    <div style={{ width: "80%", margin: "0 auto" }}>
      <CDBFileUploader />
    </div>
  );
};
