import React, { Fragment } from "react";
import CDBTable from "../components/Table";
import CDBTableHeader from "../components/Table/TableHeader";
import CDBTableBody from "../components/Table/TableBody";

export const Table = () => {
  return (
    <Fragment>
      <div
        style={{
          margin: "auto",
          width: "80%",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          minHeight: "100vh",
        }}
      >
        <div style={{ margin: "20px 0", width: "100%" }}>
          <CDBTable style={{ background: "#fff" }}>
            <CDBTableHeader>
              <tr>
                <th>#</th>
                <th>First</th>
                <th>Last</th>
                <th>Handle</th>
              </tr>
            </CDBTableHeader>
            <CDBTableBody>
              <tr>
                <td>1</td>
                <td>Mark</td>
                <td>Otto</td>
                <td>@mdo</td>
              </tr>
              <tr>
                <td>2</td>
                <td>Jacob</td>
                <td>Thornton</td>
                <td>@fat</td>
              </tr>
              <tr>
                <td>3</td>
                <td>Larry</td>
                <td>the Bird</td>
                <td>@twitter</td>
              </tr>
            </CDBTableBody>
          </CDBTable>
        </div>
      </div>
    </Fragment>
  );
};
