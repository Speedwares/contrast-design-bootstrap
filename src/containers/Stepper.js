import CDBStepper from "../components/Stepper/Stepper";
import React, { Component } from "react";
import CDBIcon from "../components/Icon";
import CDBCard from "../components/Card";
import CDBBtn from "../components/Button";
import CDBStep from "../components/Step";
import CDBCardBody from "../components/Card/CardBody";
import CDBInput from "../components/Input";
import CDBContainer from "../components/Container";

export default class Stepper extends Component {
  state = {
    formActivePanel3: 1,
    formActivePanel1Changed: false,
  };

  swapFormActive = (a) => (param) => (e) => {
    this.setState({
      ["formActivePanel" + a]: param,
      ["formActivePanel" + a + "Changed"]: true,
    });
  };

  handleNextPrevClick = (a) => (param) => (e) => {
    this.setState({
      ["formActivePanel" + a]: param,
      ["formActivePanel" + a + "Changed"]: true,
    });
  };

  handleSubmission = () => {
    alert("Form submitted!");
  };

  calculateAutofocus = (a) => {
    if (this.state["formActivePanel" + a + "Changed"]) {
      return true;
    }
  };
  render() {
    return (
      <>
        <div>
          <CDBStepper direction="horizontal">
            <CDBStep far icon="folder-open" name="Basic Information"></CDBStep>
            <CDBStep icon="pencil-alt" name="Personal Data"></CDBStep>
            <CDBStep icon="facebook" name="Terms and Conditions"></CDBStep>
            <CDBStep icon="check" name="Finish"></CDBStep>
          </CDBStepper>
        </div>
        <CDBCard>
          <CDBCardBody style={{ display: "flex", flexDirection: "row" }}>
            <div style={{ height: "500px" }}>
              <CDBStepper direction="vertical">
                <CDBStep
                  far
                  icon="folder-open"
                  name="Basic Information"
                ></CDBStep>
                <CDBStep icon="pencil-alt" name="Personal Data"></CDBStep>
                <CDBStep icon="facebook" name="Terms and Conditions"></CDBStep>
                <CDBStep icon="check" name="Finish"></CDBStep>
              </CDBStepper>
            </div>
            <div>
              {this.state.formActivePanel3 === 1 && (
                <CDBContainer md="12">
                  <h3 className="font-weight-bold pl-0 my-4">
                    <strong>Basic Information</strong>
                  </h3>
                  <CDBInput
                    label="Email"
                    className="mt-4"
                    autoFocus={this.calculateAutofocus(3)}
                  />
                  <CDBInput label="Username" className="mt-4" />
                  <CDBInput label="Password" className="mt-4" />
                  <CDBInput label="Repeat Password" className="mt-4" />
                  <CDBBtn
                    color="CDB-color"
                    rounded
                    className="float-right"
                    onClick={this.handleNextPrevClick(3)(2)}
                  >
                    next
                  </CDBBtn>
                </CDBContainer>
              )}
              {this.state.formActivePanel3 === 2 && (
                <CDBContainer md="12">
                  <h3 className="font-weight-bold pl-0 my-4">
                    <strong>Personal Data</strong>
                  </h3>
                  <CDBInput
                    label="First Name"
                    className="mt-3"
                    autoFocus={this.calculateAutofocus(3)}
                  />
                  <CDBInput label="Second Name" className="mt-3" />
                  <CDBInput label="Surname" className="mt-3" />
                  <CDBInput label="Address" type="textarea" rows="2" />
                  <CDBBtn
                    color="CDB-color"
                    rounded
                    className="float-left"
                    onClick={this.handleNextPrevClick(3)(1)}
                  >
                    previous
                  </CDBBtn>
                  <CDBBtn
                    color="CDB-color"
                    rounded
                    className="float-right"
                    onClick={this.handleNextPrevClick(3)(3)}
                  >
                    next
                  </CDBBtn>
                </CDBContainer>
              )}
              {this.state.formActivePanel3 === 3 && (
                <CDBContainer md="12">
                  <h3 className="font-weight-bold pl-0 my-4">
                    <strong>Terms and conditions</strong>
                  </h3>
                  <CDBInput
                    label="I agreee to the terms and conditions"
                    type="checkbox"
                    id="checkbox3"
                    autoFocus={this.calculateAutofocus(3)}
                  />
                  <CDBInput
                    label="I want to receive newsletter"
                    type="checkbox"
                    id="checkbox4"
                  />
                  <CDBBtn
                    color="CDB-color"
                    rounded
                    className="float-left"
                    onClick={this.handleNextPrevClick(3)(2)}
                  >
                    previous
                  </CDBBtn>
                  <CDBBtn
                    color="CDB-color"
                    rounded
                    className="float-right"
                    onClick={this.handleNextPrevClick(3)(4)}
                  >
                    next
                  </CDBBtn>
                </CDBContainer>
              )}
              {this.state.formActivePanel3 === 4 && (
                <CDBContainer md="12">
                  <h3 className="font-weight-bold pl-0 my-4">
                    <strong>Finish</strong>
                  </h3>
                  <h2 className="text-center font-weight-bold my-4">
                    Registration completed!
                  </h2>
                  <CDBBtn
                    color="CDB-color"
                    rounded
                    className="float-left"
                    onClick={this.handleNextPrevClick(3)(3)}
                  >
                    previous
                  </CDBBtn>
                  <CDBBtn
                    color="success"
                    rounded
                    className="float-right"
                    onClick={this.handleSubmission}
                  >
                    submit
                  </CDBBtn>
                </CDBContainer>
              )}
            </div>
          </CDBCardBody>
        </CDBCard>

        <div className="buttons-container"></div>
      </>
    );
  }
}
