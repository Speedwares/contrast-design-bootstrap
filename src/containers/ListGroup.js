import React, { Fragment } from "react";
import { CDBListGroup } from "../components/ListGroup";
import { CDBListGroupItem } from "../components/ListGroup/ListGroupItem";

export const ListGroup = () => {
  return (
    <Fragment>
      <div
        style={{
          margin: "auto",
          width: "80%",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          minHeight: "100vh",
        }}
      >
        <div style={{ margin: "30px 0" }}>
          <CDBListGroup>
            <CDBListGroupItem>List Group Normal</CDBListGroupItem>
            <CDBListGroupItem color="primary">
              List Group Primary
            </CDBListGroupItem>
            <CDBListGroupItem color="secondary">
              List Group Secondary
            </CDBListGroupItem>
            <CDBListGroupItem color="success">
              List Group Success
            </CDBListGroupItem>
            <CDBListGroupItem href>List Group Link</CDBListGroupItem>
            <CDBListGroupItem color="warning">
              List Group Warning
            </CDBListGroupItem>
          </CDBListGroup>
        </div>
      </div>
    </Fragment>
  );
};
